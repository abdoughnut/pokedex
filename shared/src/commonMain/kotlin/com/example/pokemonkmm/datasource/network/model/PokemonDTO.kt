package com.example.pokemonkmm.datasource.network.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


// Abilities provide passive effects for Pokémon in battle or in the overworld. Pokémon have
// multiple possible abilities but can have only one ability at a time. Check out
// [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Ability) for greater detail.
@Serializable data class AbilityDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // Whether or not this ability originated in the main series of the video games.
  @SerialName("is_main_series") val isMainSeries: Boolean,

  // The generation this ability originated in.
  @SerialName("generation") val generation: NamedApiResource,

  // The name of this resource listed in different languages.
  @SerialName("names") val names: List<NameDTO>,

  // The effect of this ability listed in different languages.
  @SerialName("effect_entries") val effectEntries: List<VerboseEffect>,

  // The list of previous effects this ability has had across version groups.
  @SerialName("effect_changes") val effectChanges: List<AbilityEffectChangeDTO>,

  // The flavor text of this ability listed in different languages.
  @SerialName("flavor_text_entries") val flavorTextEntries: List<AbilityFlavorTextDTO>,

  // A list of Pokémon that could potentially have this ability.
  @SerialName("pokemon") val pokemon: List<AbilityPokemonDTO>

)

@Serializable data class AbilityEffectChangeDTO(

  // The previous effect of this ability listed in different languages.
  @SerialName("effect_entries") val effectEntries: List<EffectDTO>,

  // The version group in which the previous effect of this ability originated.
  @SerialName("version_group") val versionGroup: NamedApiResource

)

@Serializable data class AbilityFlavorTextDTO(

  // The localized name for an API resource in a specific language.
  @SerialName("flavor_text") val flavorText: String,

  // The language this text resource is in.
  @SerialName("language") val language: NamedApiResource,

  // The version group that uses this flavor text.
  @SerialName("version_group") val versionGroup: NamedApiResource

)

@Serializable data class AbilityPokemonDTO(

  // Whether or not this a hidden ability for the referenced Pokémon.
  @SerialName("is_hidden") val isHidden: Boolean,

  // Pokémon have 3 ability 'slots' which hold references to possible abilities they could have.
  // This is the slot of this ability for the referenced pokemon.
  @SerialName("slot") val slot: Int,

  // The Pokémon this ability could belong to.
  @SerialName("pokemon") val pokemon: NamedApiResource

)

// Characteristics indicate which stat contains a Pokémon's highest IV. A Pokémon's Characteristic
// is determined by the remainder of its highest IV divided by 5 (gene_modulo).
// Check out [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Characteristic) for greater detail.
@Serializable data class CharacteristicDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The remainder of the highest stat/IV divided by 5.
  @SerialName("gene_modulo") val geneModulo: Int,

  // The possible values of the highest stat that would result in a Pokémon receiving this
  // characteristic when divided by 5.
  @SerialName("possible_values") val possibleValues: List<Int>

)

// Egg Groups are categories which determine which Pokémon are able to interbreed. Pokémon may
// belong to either one or two Egg Groups.
// Check out [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Egg_Group) for greater detail.
@Serializable data class EggGroupDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // The name of this resource listed in different languages.
  @SerialName("names") val names: List<NameDTO>,

  // A list of all Pokémon species that are members of this egg group.
  @SerialName("pokemon_species") val pokemonSpecies: List<NamedApiResource>

)

// Genders were introduced in Generation II for the purposes of breeding Pokémon but can also result
// in visual differences or even different evolutionary lines.
// Check out [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Gender) for greater detail.
@Serializable data class GenderDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // A list of Pokémon species that can be this gender and how likely it is that they will be.
  @SerialName("pokemon_species_details") val pokemonSpeciesDetails: List<PokemonSpeciesGenderDTO>,

  // A list of Pokémon species that required this gender in order for a Pokémon to evolve into them.
  @SerialName("required_for_evolution") val requiredForEvolution: List<NamedApiResource>

)

@Serializable data class PokemonSpeciesGenderDTO(

  // The chance of this Pokémon being female, in eighths; or -1 for genderless.
  @SerialName("rate") val rate: Int,

  // A Pokémon species that can be the referenced gender.
  @SerialName("pokemon_species") val pokemonSpecies: NamedApiResource

)

// Growth rates are the speed with which Pokémon gain levels through experience.
// Check out [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Experience) for greater detail.
@Serializable data class GrowthRate(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // The formula used to calculate the rate at which the Pokémon species gains level.
  @SerialName("formula") val formula: String,

  // The descriptions of this characteristic listed in different languages.
  @SerialName("descriptions") val descriptions: List<DescriptionDTO>,

  // A list of levels and the amount of experienced needed to atain them based on this growth rate.
  @SerialName("levels") val levels: List<GrowthRateExperienceLevelDTO>,

  // A list of Pokémon species that gain levels at this growth rate.
  @SerialName("pokemon_species") val pokemonSpecies: List<NamedApiResource>

)

@Serializable data class GrowthRateExperienceLevelDTO(

  // The level gained.
  @SerialName("level") val level: Int,

  // The amount of experience required to reach the referenced level.
  @SerialName("experience") val experience: Int

)

// Natures influence how a Pokémon's stats grow.
// See [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Nature) for greater detail.
@Serializable data class NatureDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // The stat decreased by 10% in Pokémon with this nature.
  @SerialName("decreased_stat") val decreasedStat: NamedApiResource?,

  // The stat increased by 10% in Pokémon with this nature.
  @SerialName("increased_stat") val increasedStat: NamedApiResource?,

  // The flavor hated by Pokémon with this nature.
  @SerialName("hates_flavor") val hatesFlavor: NamedApiResource?,

  // The flavor liked by Pokémon with this nature.
  @SerialName("likes_flavor") val likesFlavor: NamedApiResource?,

  // A list of Pokéathlon stats this nature effects and how much it effects them.
  @SerialName("pokeathlon_stat_changes") val pokeathlonStatChanges: List<NatureStatChangeDTO>,

  // The name of this resource listed in different languages.
  @SerialName("move_battle_style_preferences") val moveBattleStylePreferences: List<MoveBattleStylePreferenceDTO>,

  // The name of this resource listed in different languages.
  @SerialName("names") val names: List<NameDTO>

)

@Serializable data class NatureStatChangeDTO(

  // The amount of change.
  @SerialName("max_change") val maxChange: Int,

  // The stat being affected.
  @SerialName("pokeathlon_stat") val pokeathlonStat: NamedApiResource

)

@Serializable data class MoveBattleStylePreferenceDTO(

  // Chance of using the move, in percent, if HP is under one half.
  @SerialName("low_hp_preference") val lowHpPreference: Int,

  // Chance of using the move, in percent, if HP is over one half.
  @SerialName("high_hp_preference") val highHpPreference: Int,

  // The move battle style.
  @SerialName("move_battle_style") val moveBattleStyle: NamedApiResource

)

// Pokéathlon Stats are different attributes of a Pokémon's performance in Pokéathlons. In
// Pokéathlons, competitions happen on different courses; one for each of the different Pokéathlon
// stats. See [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Pok%C3%A9athlon) for greater
// detail.
@Serializable data class PokeathlonStatDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // The name of this resource listed in different languages.
  @SerialName("names") val names: List<NameDTO>,

  // A detail of natures which affect this Pokéathlon stat positively or negatively.
  @SerialName("affectingNatures") val affectingNatures: NaturePokeathlonStatAffectSetsDTO

)

@Serializable data class NaturePokeathlonStatAffectSetsDTO(

  // A list of natures and how they change the referenced Pokéathlon stat.
  @SerialName("increase") val increase: List<NaturePokeathlonStatAffectDTO>,

  // A list of natures and how they change the referenced Pokéathlon stat.
  @SerialName("decrease") val decrease: List<NaturePokeathlonStatAffectDTO>

)

@Serializable data class NaturePokeathlonStatAffectDTO(

  // The maximum amount of change to the referenced Pokéathlon stat.
  @SerialName("max_change") val maxChange: Int,

  // The nature causing the change.
  @SerialName("nature") val nature: NamedApiResource

)

// Pokémon are the creatures that inhabit the world of the Pokémon games. They can be caught using
// Pokéballs and trained by battling with other Pokémon. Each Pokémon belongs to a specific species
// but may take on a variant which makes it differ from other Pokémon of the same species, such as
// base stats, available abilities and typings.
// See [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Pok%C3%A9mon_(species)) for greater
// detail.
@Serializable data class PokemonDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // The base experience gained for defeating this Pokémon.
  @SerialName("base_experience") val baseExperience: Int,

  // The height of this Pokémon in decimetres.
  @SerialName("height") val height: Int,

  // Set for exactly one Pokémon used as the default for each species.
  @SerialName("is_default") val isDefault: Boolean,

  // Order for sorting. Almost national order, except families are grouped together.
  @SerialName("order") val order: Int,

  // The weight of this Pokémon in hectograms.
  @SerialName("weight") val weight: Int,

  // A list of abilities this Pokémon could potentially have.
  @SerialName("abilities") val abilities: List<PokemonAbilityDTO>,

  // A list of forms this Pokémon can take on.
  @SerialName("forms") val forms: List<NamedApiResource>,

  // A list of game indices relevant to Pokémon item by generation.
  @SerialName("game_indices") val gameIndices: List<VersionGameIndexDTO>,

  // A list of items this Pokémon may be holding when encountered.
  @SerialName("held_items") val heldItems: List<PokemonHeldItemDTO>,

  // A link to a list of location areas, as well as encounter details pertaining to specific
  // versions.
  @SerialName("location_area_encounters") val locationAreaEncounters: String,

  // A list of moves along with learn methods and level details pertaining to specific version
  // groups.
  @SerialName("moves") val moves: List<PokemonMoveDTO>,

  // A set of sprites used to depict this Pokémon in the game. A visual representation of the
  // various sprites can be found at [PokeAPI/sprites](https://github.com/PokeAPI/sprites#sprites)
  @SerialName("sprites") val sprites: PokemonSpritesDTO,

  // The species this Pokémon belongs to.
  @SerialName("species") val species: NamedApiResource,

  // A list of base stat values for this Pokémon.
  @SerialName("stats") val stats: List<PokemonStatDTO>,

  // A list of details showing types this Pokémon has.
  @SerialName("types") val types: List<PokemonTypeDTO>

)

@Serializable data class PokemonAbilityDTO(

  // Whether or not this is a hidden ability.
  @SerialName("is_hidden") val isHidden: Boolean,

  // The slot this ability occupies in this Pokémon species.
  @SerialName("slot") val slot: Int,

  // The ability the Pokémon may have.
  @SerialName("ability") val ability: NamedApiResource

)

@Serializable data class PokemonTypeDTO(

  // The order the Pokémon's types are listed in.
  @SerialName("slot") val slot: Int,

  // The type the referenced Pokémon has.
  @SerialName("type") val type: NamedApiResource

)

@Serializable data class PokemonHeldItemDTO(

  // The item the referenced Pokémon holds.
  @SerialName("item") val item: NamedApiResource,

  // The details of the different versions in which the item is held.
  @SerialName("version_details") val versionDetails: List<PokemonHeldItemVersionDTO>

)

@Serializable data class PokemonHeldItemVersionDTO(

  // The version in which the item is held.
  @SerialName("version") val version: NamedApiResource,

  // How often the item is held.
  @SerialName("rarity") val rarity: Int

)

@Serializable data class PokemonMoveDTO(

  // The move the Pokémon can learn.
  @SerialName("move") val move: NamedApiResource,

  // The details of the version in which the Pokémon can learn the move.
  @SerialName("version_group_details") val versionGroupDetails: List<PokemonMoveVersionDTO>

)

@Serializable data class PokemonMoveVersionDTO(

  // The method by which the move is learned.
  @SerialName("move_learn_method") val moveLearnMethod: NamedApiResource,

  // The version group in which the move is learned.
  @SerialName("version_group") val versionGroup: NamedApiResource,

  // The minimum level to learn the move.
  @SerialName("level_learned_at") val levelLearnedAt: Int

)

@Serializable data class PokemonStatDTO(

  // The stat the Pokémon has.
  @SerialName("stat") val stat: NamedApiResource,

  // The effort points (EV) the Pokémon has in the stat.
  @SerialName("effort") val effort: Int,

  // The base value of the stat.
  @SerialName("base_stat") val baseStat: Int

)

@Serializable data class PokemonSpritesDTO(

  // The default depiction of this Pokémon from the back in battle.
  @SerialName("back_default") val backDefault: String?,

  // The shiny depiction of this Pokémon from the back in battle.
  @SerialName("back_shiny") val backShiny: String?,

  // The female depiction of this Pokémon from the back in battle.
  @SerialName("back_female") val backFemale: String?,

  // The shiny female depiction of this Pokémon from the back in battle.
  @SerialName("back_shiny_female") val backShinyFemale: String?,

  // The default depiction of this Pokémon from the front in battle.
  @SerialName("front_default") val frontDefault: String?,

  // The shiny depiction of this Pokémon from the front in battle.
  @SerialName("front_shiny") val frontShiny: String?,

  // The female depiction of this Pokémon from the front in battle.
  @SerialName("front_female") val frontFemale: String?,

  // The shiny female depiction of this Pokémon from the front in battle.
  @SerialName("front_shiny_female") val frontShinyFemale: String?

)

// Pokémon Location Areas are ares where Pokémon can be found.
@Serializable data class LocationAreaEncounterDTO(

  // The location area the referenced Pokémon can be encountered in.
  @SerialName("location_area") val locationArea: NamedApiResource,

  // A list of versions and encounters with the referenced Pokémon that might happen.
  @SerialName("version_details") val versionDetails: List<VersionEncounterDetailDTO>

)

// Colors used for sorting Pokémon in a Pokédex. The color listed in the Pokédex is usually the
// color most apparent or covering each Pokémon's body. No orange category exists; Pokémon that are
// primarily orange are listed as red or brown.
@Serializable data class PokemonColorDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // The name of this resource listed in different languages.
  @SerialName("names") val names: List<NameDTO>,

  // A list of the Pokémon species that have this color.
  @SerialName("pokemon_species") val pokemonSpecies: List<NamedApiResource>

)

// Some Pokémon may appear in one of multiple, visually different forms. These differences are
// purely cosmetic. For variations within a Pokémon species, which do differ in more than just
// visuals, the 'Pokémon' entity is used to represent such a variety.
@Serializable data class PokemonFormDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // The order in which forms should be sorted within all forms. Multiple forms may have equal
  // order, in which case they should fall back on sorting by name.
  @SerialName("order") val order: Int,

  // The order in which forms should be sorted within a species' forms.
  @SerialName("form_order") val formOrder: Int,

  // True for exactly one form used as the default for each Pokémon.
  @SerialName("is_default") val isDefault: Boolean,

  // Whether or not this form can only happen during battle.
  @SerialName("is_battle_only") val isBattleOnly: Boolean,

  // Whether or not this form requires mega evolution.
  @SerialName("is_mega") val isMega: Boolean,

  // The name of this form.
  @SerialName("form_name") val formName: String,

  // The Pokémon that can take on this form.
  @SerialName("pokemon") val pokemon: NamedApiResource,

  // A set of sprites used to depict this Pokémon form in the game.
  @SerialName("sprites") val sprites: PokemonFormSpritesDTO,

  // The version group this Pokémon form was introduced in.
  @SerialName("version_group") val versionGroup: NamedApiResource,

  // The form specific form name of this Pokémon form, or empty if the form does not have a specific
  // name.
  @SerialName("form_names") val formNames: List<NameDTO>

)

@Serializable data class PokemonFormSpritesDTO(

  // The default depiction of this Pokémon form from the back in battle.
  @SerialName("back_default") val backDefault: String?,

  // The shiny depiction of this Pokémon form from the back in battle.
  @SerialName("back_shiny") val backShiny: String?,

  // The default depiction of this Pokémon form from the front in battle.
  @SerialName("front_default") val frontDefault: String?,

  // The shiny depiction of this Pokémon form from the front in battle.
  @SerialName("front_shiny") val frontShiny: String?

)

// Habitats are generally different terrain Pokémon can be found in but can also be areas designated
// for rare or legendary Pokémon.
@Serializable data class PokemonHabitatDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // The name of this resource listed in different languages.
  @SerialName("names") val names: List<NameDTO>,

  // A list of the Pokémon species that can be found in this habitat.
  @SerialName("pokemon_species") val pokemonSpecies: List<NamedApiResource>

)

// Shapes used for sorting Pokémon in a Pokédex.
@Serializable data class PokemonShapeDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // The "scientific" name of this Pokémon shape listed in different languages.
  @SerialName("awesome_names") val awesomeNames: List<AwesomeNameDTO>,

  // The name of this resource listed in different languages.
  @SerialName("names") val names: List<NameDTO>,

  // A list of the Pokémon species that have this shape.
  @SerialName("pokemonSpecies") val pokemonSpecies: List<NamedApiResource>

)

@Serializable data class AwesomeNameDTO(

  // The localized "scientific" name for an API resource in a specific language.
  @SerialName("awesome_name") val awesomeName: String,

  // The language this "scientific" name is in.
  @SerialName("language") val language: NamedApiResource

)

// A Pokémon Species forms the basis for at least one Pokémon. Attributes of a Pokémon species are
// shared across all varieties of Pokémon within the species. A good example is Wormadam; Wormadam
// is the species which can be found in three different varieties, Wormadam-Trash, Wormadam-Sandy
// and Wormadam-Plant.
@Serializable data class PokemonSpeciesDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // The order in which species should be sorted. Based on National Dex order, except families are
  // grouped together and sorted by stage.
  @SerialName("order") val order: Int,

  // The chance of this Pokémon being female, in eighths; or -1 for genderless.
  @SerialName("gender_rate") val genderRate: Int,

  // The base capture rate; up to 255. The higher the number, the easier the catch.
  @SerialName("capture_rate") val captureRate: Int,

  // The happiness when caught by a normal Pokéball; up to 255. The higher the number, the happier
  // the Pokémon.
  @SerialName("base_happiness") val baseHappiness: Int,

  // Whether or not this is a baby Pokémon.
  @SerialName("is_baby") val isBaby: Boolean,

  // Whether or not this is a legendary Pokémon.
  @SerialName("is_legendary") val isLegendary: Boolean,

  // Whether or not this is a mythical Pokémon.
  @SerialName("is_mythical") val isMythical: Boolean,

  // Initial hatch counter: one must walk 255 × (hatch_counter + 1) steps before this Pokémon's egg
  // hatches, unless utilizing bonuses like Flame Body's.
  @SerialName("hatch_counter") val hatchCounter: Int,

  // Whether or not this Pokémon has visual gender differences.
  @SerialName("has_gender_differences") val hasGenderDifferences: Boolean,

  // Whether or not this Pokémon has multiple forms and can switch between them.
  @SerialName("forms_switchable") val formsSwitchable: Boolean,

  // The rate at which this Pokémon species gains levels.
  @SerialName("growth_rate") val growthRate: NamedApiResource,

  // A list of Pokedexes and the indexes reserved within them for this Pokémon species.
  @SerialName("pokedex_numbers") val pokedexNumbers: List<PokemonSpeciesDexEntryDTO>,

  // A list of egg groups this Pokémon species is a member of.
  @SerialName("egg_groups") val eggGroups: List<NamedApiResource>,

  // The color of this Pokémon for Pokédex search.
  @SerialName("color") val color: NamedApiResource,

  // The shape of this Pokémon for Pokédex search.
  @SerialName("shape") val shape: NamedApiResource,

  // The Pokémon species that evolves into this Pokemon_species.
  @SerialName("evolves_from_species") val evolvesFromSpecies: NamedApiResource?,

  // The evolution chain this Pokémon species is a member of.
  @SerialName("evolution_chain") val evolutionChain: ApiResource,

  // The habitat this Pokémon species can be encountered in.
  @SerialName("habitat") val habitat: NamedApiResource?,

  // The generation this Pokémon species was introduced in.
  @SerialName("generation") val generation: NamedApiResource,

  // The name of this resource listed in different languages.
  @SerialName("names") val names: List<NameDTO>,

  // A list of encounters that can be had with this Pokémon species in pal park.
  @SerialName("pal_park_encounters") val palParkEncounters: List<PalParkEncounterAreaDTO>,

  // A list of flavor text entries for this Pokémon species.
  @SerialName("flavor_text_entries") val flavorTextEntries: List<FlavorTextDTO>,

  // Descriptions of different forms Pokémon take on within the Pokémon species.
  @SerialName("form_descriptions") val formDescriptions: List<DescriptionDTO>,

  // The genus of this Pokémon species listed in multiple languages.
  @SerialName("genera") val genera: List<GenusDTO>,

  // A list of the Pokémon that exist within this Pokémon species.
  @SerialName("varieties") val varieties: List<PokemonSpeciesVarietyDTO>

)

@Serializable data class GenusDTO(

  // The localized genus for the referenced Pokémon species
  @SerialName("genus") val genus: String,

  // The language this genus is in.
  @SerialName("language") val language: NamedApiResource

)

@Serializable data class PokemonSpeciesDexEntryDTO(

  // The index number within the Pokédex.
  @SerialName("entry_number") val entryNumber: Int,

  // The Pokédex the referenced Pokémon species can be found in.
  @SerialName("pokedex") val pokedex: NamedApiResource

)

@Serializable data class PalParkEncounterAreaDTO(

  // The base score given to the player when the referenced Pokémon is caught during a pal park run.
  @SerialName("base_score") val baseScore: Int,

  // The base rate for encountering the referenced Pokémon in this pal park area.
  @SerialName("rate") val rate: Int,

  // The pal park area where this encounter happens.
  @SerialName("area") val area: NamedApiResource

)

@Serializable data class PokemonSpeciesVarietyDTO(

  // Whether this variety is the default variety.
  @SerialName("is_default") val isDefault: Boolean,

  // The Pokémon variety.
  @SerialName("pokemon") val pokemon: NamedApiResource

)

// Stats determine certain aspects of battles. Each Pokémon has a value for each stat which grows as
// they gain levels and can be altered momentarily by effects in battles.
@Serializable data class StatDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // ID the games use for this stat.
  @SerialName("game_index") val gameIndex: Int,

  // Whether this stat only exists within a battle.
  @SerialName("is_battle_only") val isBattleOnly: Boolean,

  // A detail of moves which affect this stat positively or negatively.
  @SerialName("affecting_moves") val affectingMoves: MoveStatAffectSetsDTO,

  // A detail of natures which affect this stat positively or negatively.
  @SerialName("affecting_natures") val affectingNatures: NatureStatAffectSetsDTO,

  // A list of characteristics that are set on a Pokémon when its highest base stat is this stat.
  @SerialName("characteristics") val characteristics: List<ApiResource>,

  // The class of damage this stat is directly related to.
  @SerialName("move_damage_class") val moveDamageClass: NamedApiResource?,

  // The name of this resource listed in different languages.
  @SerialName("names") val names: List<NameDTO>

)

@Serializable data class MoveStatAffectSetsDTO(

  // A list of natures and how they change the referenced stat.
  @SerialName("increase") val increase: List<MoveStatAffectDTO>,

  // A list of nature sand how they change the referenced stat.
  @SerialName("decrease") val decrease: List<MoveStatAffectDTO>

)

@Serializable data class MoveStatAffectDTO(

  // // The maximum amount of change to the referenced stat.
  @SerialName("change") val change: Int,

  // // The move causing the change.
  @SerialName("move") val move: NamedApiResource
)

@Serializable data class NatureStatAffectSetsDTO(

  // A list of natures and how they change the referenced stat.
  @SerialName("increase") val increase: List<NamedApiResource>,

  // A list of nature sand how they change the referenced stat.
  @SerialName("decrease") val decrease: List<NamedApiResource>

)

// Types are properties for Pokémon and their moves. Each type has three properties: which types of
// Pokémon it is super effective against, which types of Pokémon it is not very effective against,
// and which types of Pokémon it is completely ineffective against.
@Serializable data class TypeDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // A detail of how effective this type is toward others and vice versa.
  @SerialName("damage_relations") val damageRelations: TypeRelationsDTO,

  // A list of game indices relevant to this item by generation.
  @SerialName("game_indices") val gameIndices: List<GenerationGameIndexDTO>,

  // The generation this type was introduced in.
  @SerialName("generation") val generation: NamedApiResource,

  // The class of damage inflicted by this type.
  @SerialName("move_damage_class") val moveDamageClass: NamedApiResource?,

  // The name of this resource listed in different languages.
  @SerialName("names") val names: List<NameDTO>,

  // A list of details of Pokémon that have this type.
  @SerialName("pokemon") val pokemon: List<TypePokemonDTO>,

  // A list of moves that have this type.
  @SerialName("moves") val moves: List<NamedApiResource>

)

@Serializable data class TypePokemonDTO(

  // The order the Pokémon's types are listed in.
  @SerialName("slot") val slot: Int,

  // The Pokémon that has the referenced type.
  @SerialName("pokemon") val pokemon: NamedApiResource

)

@Serializable data class TypeRelationsDTO(

  // A list of types this type has no effect on.
  @SerialName("no_damage_to") val noDamageTo: List<NamedApiResource>,

  // A list of types this type is not very effect against.
  @SerialName("half_damage_to") val halfDamageTo: List<NamedApiResource>,

  // A list of types this type is very effect against.
  @SerialName("double_damage_to") val doubleDamageTo: List<NamedApiResource>,

  // A list of types that have no effect on this type.
  @SerialName("no_damage_from") val noDamageFrom: List<NamedApiResource>,

  // A list of types that are not very effective against this type.
  @SerialName("half_damage_from") val halfDamageFrom: List<NamedApiResource>,

  // A list of types that are very effective against this type.
  @SerialName("double_damage_from") val doubleDamageFrom: List<NamedApiResource>

)