package com.example.pokemonkmm.datasource.network.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


// Locations that can be visited within the games. Locations make up sizable portions of regions,
// like cities or routes.
@Serializable data class LocationDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // The region this location can be found in.
  @SerialName("region") val region: NamedApiResource?,

  // The name of this resource listed in different languages.
  @SerialName("names") val names: List<NameDTO>,

  // A list of game indices relevant to this location by generation.
  @SerialName("game_indices") val gameIndices: List<GenerationGameIndexDTO>,

  // Areas that can be found within this location.
  @SerialName("areas") val areas: List<NamedApiResource>

)

// Location areas are sections of areas, such as floors in a building or cave. Each area has its own
// set of possible Pokémon encounters.
@Serializable data class LocationAreaDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // The internal id of an API resource within game data.
  @SerialName("game_index") val gameIndex: Int,

  // A list of methods in which Pokémon may be encountered in this area and how likely the method
  // will occur depending on the version of the game.
  @SerialName("encounter_method_rates") val encounterMethodRates: List<EncounterMethodRateDTO>,

  // The region this location area can be found in.
  @SerialName("location") val location: NamedApiResource,

  // The name of this resource listed in different languages.
  @SerialName("names") val names: List<NameDTO>,

  // A list of Pokémon that can be encountered in this area along with version specific details
  // about the encounter.
  @SerialName("pokemon_encounters") val pokemonEncounters: List<PokemonEncounterDTO>

)

@Serializable data class EncounterMethodRateDTO(

  // The method in which Pokémon may be encountered in an area..
  @SerialName("encounter_method") val encounterMethod: NamedApiResource,

  // The chance of the encounter to occur on a version of the game.
  @SerialName("version_details") val versionDetails: List<EncounterMethodRateVersionDetailDTO>

)

@Serializable data class EncounterMethodRateVersionDetailDTO(

  // The chance of an encounter to occur.
  @SerialName("rate") val rate: Int,

  // The version of the game in which the encounter can occur with the given chance.
  @SerialName("version") val version: NamedApiResource

)

@Serializable data class PokemonEncounterDTO(

  // The Pokémon being encountered.
  @SerialName("pokemon") val pokemon: NamedApiResource,

  // A list of versions and encounters with Pokémon that might happen in the referenced location
  // area.
  @SerialName("versionDetails") val versionDetails: List<VersionEncounterDetailDTO>

)

// Areas used for grouping Pokémon encounters in Pal Park. They're like habitats that are specific
// to [Pal Park](https://bulbapedia.bulbagarden.net/wiki/Pal_Park).
@Serializable data class PalParkAreaDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // The name of this resource listed in different languages.
  @SerialName("names") val names: List<NameDTO>,

  // The name of this resource listed in different languages.
  @SerialName("pokemon_encounters") val pokemonEncounters: List<PalParkEncounterSpeciesDTO>

)

@Serializable data class PalParkEncounterSpeciesDTO(

  // The base score given to the player when this Pokémon is caught during a pal park run.
  @SerialName("base_score") val baseScore: Int,

  // The base rate for encountering this Pokémon in this pal park area.
  @SerialName("rate") val rate: Int,

  // The Pokémon species being encountered.
  @SerialName("pokemon_species") val pokemonSpecies: NamedApiResource

)

// A region is an organized area of the Pokémon world. Most often, the main difference between
// regions is the species of Pokémon that can be encountered within them.
@Serializable data class RegionDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // A list of locations that can be found in this region.
  @SerialName("locations") val locations: List<NamedApiResource>,

  // The name for this resource.
  @SerialName("name") val name: String,

  // The name of this resource listed in different languages.
  @SerialName("names") val names: List<NameDTO>,

  // The generation this region was introduced in.
  @SerialName("main_generation") val mainGeneration: NamedApiResource,

  // A list of pokédexes that catalogue Pokémon in this region.
  @SerialName("pokedexes") val pokedexes: List<NamedApiResource>,

  // A list of version groups where this region can be visited.
  @SerialName("version_groups") val versionGroups: List<NamedApiResource>

)