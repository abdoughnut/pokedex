package com.example.pokemonkmm.datasource.network

import com.example.pokemonkmm.domain.model.Pokemon


interface PokemonService {

  suspend fun search(page: Int): List<Pokemon>

  suspend fun get(id: Int): Pokemon

}