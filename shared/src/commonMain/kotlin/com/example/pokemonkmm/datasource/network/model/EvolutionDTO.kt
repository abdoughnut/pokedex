package com.example.pokemonkmm.datasource.network.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

// Evolution chains are essentially family trees. They start with the lowest stage within a family
// and detail evolution conditions for each as well as Pokémon they can evolve into up through the
// hierarchy.
@Serializable data class EvolutionChainDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The item that a Pokémon would be holding when mating that would trigger the egg hatching a baby
  // Pokémon rather than a basic Pokémon.
  @SerialName("baby_trigger_item") val babyTriggerItem: NamedApiResource?,

  // The base chain link object. Each link contains evolution details for a Pokémon in the chain.
  // Each link references the next Pokémon in the natural evolution order.
  @SerialName("chain") val chain: ChainLinkDTO

)

@Serializable data class ChainLinkDTO(

  // Whether or not this link is for a baby Pokémon. This would only ever be true on the base link.
  @SerialName("is_baby") val isBaby: Boolean,

  // The Pokémon species at this point in the evolution chain.
  @SerialName("species") val species: NamedApiResource,

  // All details regarding the specific details of the referenced Pokémon species evolution.
  @SerialName("evolution_details") val evolutionDetails: List<EvolutionDetailDTO>,

  // A List of chain objects.
  @SerialName("evolves_to") val evolvesTo: List<ChainLinkDTO>

)

@Serializable data class EvolutionDetailDTO(
  // The item required to cause evolution this into Pokémon species.
  @SerialName("item") val item: NamedApiResource? = null,

  // The type of event that triggers evolution into this Pokémon species.
  @SerialName("trigger") val trigger: NamedApiResource,

  // The id of the gender of the evolving Pokémon species must be in order to evolve into this
  // Pokémon species.
  @SerialName("gender") val gender: Int? = null,

  // The item the evolving Pokémon species must be holding during the evolution trigger event to
  // evolve into this Pokémon species.
  @SerialName("held_item") val heldItem: NamedApiResource? = null,

  // The move that must be known by the evolving Pokémon species during the evolution trigger event
  // in order to evolve into this Pokémon species.
  @SerialName("known_move") val knownMove: NamedApiResource? = null,

  // The evolving Pokémon species must know a move with this type during the evolution trigger event
  // in order to evolve into this Pokémon species.
  @SerialName("known_move_type") val knownMoveType: NamedApiResource? = null,

  // The location the evolution must be triggered at.
  @SerialName("location") val location: NamedApiResource? = null,

  // The minimum required level of the evolving Pokémon species to evolve into this Pokémon species.
  @SerialName("min_level") val minLevel: Int? = null,

  // The minimum required level of happiness the evolving Pokémon species to evolve into this
  // Pokémon species.
  @SerialName("min_happiness") val minHappiness: Int? = null,

  // The minimum required level of beauty the evolving Pokémon species to evolve into this Pokémon
  // species
  @SerialName("min_beauty") val minBeauty: Int? = null,

  // The minimum required level of affection the evolving Pokémon species to evolve into this
  // Pokémon species.
  @SerialName("min_affection") val minAffection: Int? = null,

  // Whether or not it must be raining in the overworld to cause evolution this Pokémon species.
  @SerialName("needs_overworld_rain") val needsOverworldRain: Boolean = false,

  // The Pokémon species that must be in the players party in order for the evolving Pokémon species
  // to evolve into this Pokémon species.
  @SerialName("party_species") val partySpecies: NamedApiResource? = null,

  // The player must have a Pokémon of this type in their party during the evolution trigger event
  // in order for the evolving Pokémon species to evolve into this Pokémon species.
  @SerialName("party_type") val partyType: NamedApiResource? = null,

  // The required relation between the Pokémon's Attack and Defense stats.
  // 1 means Attack > Defense. 0 means Attack = Defense. -1 means Attack < Defense.
  @SerialName("relative_physical_stats") val relativePhysicalStats: Int? = null,

  // The required time of day. Day or night.
  @SerialName("time_of_day") val timeOfDay: String = "",

  // Pokémon species for which this one must be traded.
  @SerialName("trade_species") val tradeSpecies: NamedApiResource? = null,

  // Whether or not the 3DS needs to be turned upside-down as this Pokémon levels up.
  @SerialName("turn_upside_down") val turnUpsideDown: Boolean = false

)

// Evolution triggers are the events and conditions that cause a Pokémon to evolve. Check out
// [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Methods_of_evolution) for greater detail.
@Serializable data class EvolutionTriggerDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // The name of this resource listed in different languages.
  @SerialName("names") val names: List<NameDTO>,

  // A list of pokemon species that result from this evolution trigger.
  @SerialName("pokemon_species") val pokemonSpecies: List<NamedApiResource>

)