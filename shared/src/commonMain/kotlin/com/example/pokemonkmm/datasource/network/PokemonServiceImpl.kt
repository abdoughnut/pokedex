package com.example.pokemonkmm.datasource.network

import com.example.pokemonkmm.datasource.network.model.PokemonDTO
import com.example.pokemonkmm.domain.model.Pokemon
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.http.takeFrom


class PokemonServiceImpl(
  private val httpClient: HttpClient,
  private val baseUrl: String
) : PokemonService {

  override suspend fun search(page: Int): List<Pokemon> {
    val offset = if (page == 1) 0 else page - 1 * 20
    return httpClient.get<List<PokemonDTO>> {
      url {
        takeFrom(baseUrl)
        encodedPath = "api/v2/pokemon/?limit=20&offset=$offset"
      }
    }.toPokemonList()
  }


  override suspend fun get(id: Int): Pokemon {
    return httpClient.get<PokemonDTO> {
      url {
        takeFrom(baseUrl)
        encodedPath = "api/v2/pokemon/$id"
      }
    }.toPokemon()
  }

  companion object {
    const val BASE_URL = "https://pokeapi.co/"
  }

}