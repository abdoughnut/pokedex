package com.example.pokemonkmm.datasource.network.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


// Languages for translations of API resource information.
@Serializable data class LanguageDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // Whether or not the games are published in this language.
  @SerialName("official") val official: Boolean,

  // The two-letter code of the country where this language is spoken. Note that it is not unique.
  @SerialName("iso639") val iso639: String,

  // The two-letter code of the language. Note that it is not unique.
  @SerialName("iso3166") val iso3166: String,

  // The name of this resource listed in different languages.
  @SerialName("names") val names: List<NameDTO>

)

@Serializable data class DescriptionDTO(

  // The localized description for an API resource in a specific language.
  @SerialName("description") val description: String,

  // The language this name is in.
  @SerialName("language") val language: NamedApiResource

)

@Serializable data class EffectDTO(

  // The localized effect text for an API resource in a specific language.
  @SerialName("effect") val effect: String,

  // The language this effect is in.
  @SerialName("language") val language: NamedApiResource

)

@Serializable data class EncounterDTO(

  // The lowest level the Pokémon could be encountered at.
  @SerialName("min_level") val minLevel: Int,

  // The highest level the Pokémon could be encountered at.
  @SerialName("max_level") val maxLevel: Int,

  // A list of condition values that must be in effect for this encounter to occur.
  @SerialName("condition_values") val conditionValues: List<NamedApiResource>,

  // Percent chance that this encounter will occur.
  @SerialName("chance") val chance: Int,

  // The method by which this encounter happens.
  @SerialName("method") val method: NamedApiResource

)

@Serializable data class FlavorTextDTO(

  // The localized flavor text for an API resource in a specific language.
  @SerialName("flavor_text") val flavorText: String,

  // The language this name is in.
  @SerialName("language") val language: NamedApiResource,

  // The game version this flavor text is extracted from.
  @SerialName("version") val version: NamedApiResource

)

@Serializable data class GenerationGameIndexDTO(

  // The internal id of an API resource within game data.
  @SerialName("game_index") val gameIndex: Int,

  // The generation relevant to this game index.
  @SerialName("generation") val generation: NamedApiResource

)

@Serializable data class MachineVersionDetailDTO(

  // The machine that teaches a move from an item.
  @SerialName("machine") val machine: ApiResource,

  // The version group of this specific machine.
  @SerialName("version_group") val versionGroup: NamedApiResource

)

@Serializable data class NameDTO(

  // The localized name for an API resource in a specific language.
  @SerialName("name") val name: String,

  // The language this name is in.
  @SerialName("language") val language: NamedApiResource

)

@Serializable data class VerboseEffect(

  // The localized effect text for an API resource in a specific language.
  @SerialName("effect") val effect: String,

  // The localized effect text in brief.
  @SerialName("short_effect") val shortEffect: String,

  // The language this effect is in.
  @SerialName("language") val language: NamedApiResource

)

@Serializable data class VersionEncounterDetailDTO(

  // The game version this encounter happens in.
  @SerialName("version") val version: NamedApiResource,

  // The total percentage of all encounter potential.
  @SerialName("max_chance") val maxChance: Int,

  // A list of encounters and their specifics.
  @SerialName("encounter_details") val encounterDetails: List<EncounterDTO>

)

@Serializable data class VersionGameIndexDTO(

  // The internal id of an API resource within game data.
  @SerialName("game_index") val gameIndex: Int,

  // The version relevant to this game index.
  @SerialName("version") val version: NamedApiResource

)

@Serializable data class VersionGroupFlavorTextDTO(

  // The localized name for an API resource in a specific language.
  @SerialName("text") val text: String,

  // The language this name is in.
  @SerialName("language") val language: NamedApiResource,

  // The version group which uses this flavor text.
  @SerialName("version_group") val versionGroup: NamedApiResource

)