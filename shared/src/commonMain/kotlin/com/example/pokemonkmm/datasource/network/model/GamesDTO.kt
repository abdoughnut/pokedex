package com.example.pokemonkmm.datasource.network.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


// A generation is a grouping of the Pokémon games that separates them based on the Pokémon they
// include. In each generation, a new set of Pokémon, Moves, Abilities and Types that did not exist
// in the previous generation are released.
@Serializable data class GenerationDTO(

  // The identifier for this resource.
  @SerialName("id:") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // A list of abilities that were introduced in this generation.
  @SerialName("abilities") val abilities: List<NamedApiResource>,

  // The name of this resource listed in different languages.
  @SerialName("names") val names: List<NameDTO>,

  // The main region travelled in this generation.
  @SerialName("main_region") val mainRegion: NamedApiResource,

  // A list of moves that were introduced in this generation.
  @SerialName("moves") val moves: List<NamedApiResource>,

  // A list of Pokémon species that were introduced in this generation.
  @SerialName("pokemon_species") val pokemonSpecies: List<NamedApiResource>,

  // A list of types that were introduced in this generation.
  @SerialName("types") val types: List<NamedApiResource>,

  // A list of version groups that were introduced in this generation.
  @SerialName("version_groups") val versionGroups: List<NamedApiResource>

)

// A Pokédex is a handheld electronic encyclopedia device; one which is capable of recording and
// retaining information of the various Pokémon in a given region with the exception of the national
// dex and some smaller dexes related to portions of a region.
// See [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Pokedex) for greater detail.
@Serializable data class PokedexDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // Whether or not this Pokédex originated in the main series of the video games.
  @SerialName("is_main_series") val isMainSeries: Boolean,

  // The description of this resource listed in different languages.
  @SerialName("descriptions") val descriptions: List<DescriptionDTO>,

  // The name of this resource listed in different languages.
  @SerialName("names") val names: List<NameDTO>,

  // A list of Pokémon catalogued in this Pokédex and their indexes.
  @SerialName("pokemon_entries") val pokemonEntries: List<PokemonEntryDTO>,

  // The region this Pokédex catalogues Pokémon for.
  @SerialName("region") val region: NamedApiResource?,

  // A list of version groups this Pokédex is relevant to.
  @SerialName("version_groups") val versionGroups: List<NamedApiResource>

)

@Serializable data class PokemonEntryDTO(

  // The index of this Pokémon species entry within the Pokédex.
  @SerialName("entry_number") val entryNumber: Int,

  // The Pokémon species being encountered.
  @SerialName("pokemon_species") val pokemonSpecies: NamedApiResource

)

// Versions of the games, e.g., Red, Blue or Yellow.
@Serializable data class VersionDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // The name of this resource listed in different languages.
  @SerialName("names") val names: List<NameDTO>,

  // The version group this version belongs to.
  @SerialName("version_group") val versionGroup: NamedApiResource

)

// Version groups categorize highly similar versions of the games.
@Serializable data class VersionGroupDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // Order for sorting. Almost by date of release, except similar versions are grouped together.
  @SerialName("order") val order: Int,

  // The generation this version was introduced in.
  @SerialName("generation") val generation: NamedApiResource,

  // A list of methods in which Pokémon can learn moves in this version group.
  @SerialName("move_learn_methods") val moveLearnMethods: List<NamedApiResource>,

  // A list of Pokédexes introduces in this version group.
  @SerialName("pokedexes") val pokedexes: List<NamedApiResource>,

  // A list of Pokédexes introduces in this version group.
  @SerialName("regions") val regions: List<NamedApiResource>,

  // The versions this version group owns.
  @SerialName("versions") val versions: List<NamedApiResource>

)