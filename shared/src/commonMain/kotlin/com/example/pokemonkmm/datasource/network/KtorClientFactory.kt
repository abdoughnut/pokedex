package com.example.pokemonkmm.datasource.network

import com.example.pokemonkmm.datasource.network.model.PokemonDTO
import com.example.pokemonkmm.datasource.network.model.PokemonSpritesDTO
import com.example.pokemonkmm.domain.model.Pokemon
import com.example.pokemonkmm.domain.model.PokemonSprites
import io.ktor.client.HttpClient


expect class KtorClientFactory {
  fun build(): HttpClient
}

fun PokemonDTO.toPokemon(): Pokemon {
  return Pokemon(
    id = id,
    name = name,
    baseExperience = baseExperience,
    height = height,
    order = order,
    weight = weight,
    sprites = sprites.toPokemonSprites()
  )
}

fun List<PokemonDTO>.toPokemonList(): List<Pokemon> {
  return map { it.toPokemon() }
}

fun PokemonSpritesDTO.toPokemonSprites(): PokemonSprites {
  return PokemonSprites(
    backDefault = backDefault,
    backShiny = backShiny,
    backFemale = backFemale,
    backShinyFemale = backShinyFemale,
    frontDefault = frontDefault,
    frontShiny = frontShiny,
    frontFemale = frontFemale,
    frontShinyFemale = frontShinyFemale,
  )
}
