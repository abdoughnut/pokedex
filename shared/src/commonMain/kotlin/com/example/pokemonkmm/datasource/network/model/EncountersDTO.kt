package com.example.pokemonkmm.datasource.network.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

// Methods by which the player might can encounter Pokémon in the wild, e.g., walking in tall grass.
// Check out [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Wild_Pok%C3%A9mon) for greater detail.
@Serializable data class EncounterMethodDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // A good value for sorting.
  @SerialName("order") val order: Int,

  // The name of this resource listed in different languages.
  @SerialName("names") val names: List<NameDTO>

)

// Conditions which affect what pokemon might appear in the wild, e.g., day or night.
@Serializable data class EncounterConditionDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // The name of this resource listed in different languages.
  @SerialName("names") val names: List<NameDTO>,

  // A  list of possible values for this encounter condition.
  @SerialName("values")  val values: List<NamedApiResource>

)

@Serializable data class EncounterConditionValueDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // The condition this encounter condition value pertains to
  @SerialName("condition") val condition: NamedApiResource,

  // The name of this resource listed in different languages.
  @SerialName("names") val names: List<NameDTO>

)