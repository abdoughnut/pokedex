package com.example.pokemonkmm.datasource.network.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

// Moves are the skills of Pokémon in battle. In battle, a Pokémon uses one move each turn. Some
// moves (including those learned by Hidden Machine) can be used outside of battle as well, usually
// for the purpose of removing obstacles or exploring new areas.
@Serializable data class MoveDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // The percent value of how likely this move is to be successful.
  @SerialName("accuracy") val accuracy: Int?,

  // The percent value of how likely it is this moves effect will happen.
  @SerialName("effect_chance") val effectChance: Int?,

  // Power points. The number of times this move can be used.
  @SerialName("pp") val pp: Int?,

  // A value between -8 and 8. Sets the order in which moves are executed during battle. See
  // [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Priority) for greater detail.
  @SerialName("priority") val priority: Int,

  // The base power of this move with a value of 0 if it does not have a base power.
  @SerialName("power") val power: Int?,

  // A detail of normal and super contest combos that require this move.
  @SerialName("contest_combos") val contestCombos: ContestComboSetsDTO?,

  // The type of appeal this move gives a Pokémon when used in a contest.
  @SerialName("contest_type") val contestType: NamedApiResource?,

  // The effect the move has when used in a contest.
  @SerialName("contest_effect") val contestEffect: ApiResource?,

  // The type of damage the move inflicts on the target, e.g. physical.
  @SerialName("damage_class") val damageClass: NamedApiResource,

  // The effect of this move listed in different languages.
  @SerialName("effect_entries") val effectEntries: List<VerboseEffect>,

  // The list of previous effects this move has had across version groups of the games.
  @SerialName("effect_changes") val effectChanges: List<AbilityEffectChangeDTO>,

  // The flavor text of this move listed in different languages.
  @SerialName("flavor_text_entries") val flavorTextEntries: List<MoveFlavorTextDTO>,

  // The generation in which this move was introduced.
  @SerialName("generation") val generation: NamedApiResource,

  // A list of the machines that teach this move.
  @SerialName("machines") val machines: List<MachineVersionDetailDTO>,

  // Metadata about this move
  @SerialName("meta") val meta: MoveMetaDataDTO?,

  // The name of this resource listed in different languages.
  @SerialName("names") val names: List<NameDTO>,

  // A list of move resource value changes across version groups of the game.
  @SerialName("past_values") val pastValues: List<PastMoveStatValuesDTO>,

  // A list of stats this moves effects and how much it effects them.
  @SerialName("stat_changes") val statChanges: List<MoveStatChangeDTO>,

  // The effect the move has when used in a super contest.
  @SerialName("super_contest_effect") val superContestEffect: ApiResource?,

  // The type of target that will receive the effects of the attack.
  @SerialName("target") val target: NamedApiResource,

  // The elemental type of this move.
  @SerialName("type") val type: NamedApiResource

)

@Serializable data class ContestComboSetsDTO(

  // A detail of moves this move can be used before or after, granting additional appeal points in
  // contests.
  @SerialName("normal") val normalSet: ContestComboDetailDTO,

  // A detail of moves this move can be used before or after, granting additional appeal points in
  // super contests.
  @SerialName("super") val superSet: ContestComboDetailDTO

)

@Serializable data class ContestComboDetailDTO(

  // A list of moves to use before this move.
  @SerialName("use_before") val useBefore: List<NamedApiResource>?,

  // A list of moves to use after this move.
  @SerialName("use_after") val useAfter: List<NamedApiResource>?

)

@Serializable data class MoveFlavorTextDTO(

  // The localized flavor text for an api resource in a specific language.
  @SerialName("flavor_text") val flavorText: String,

  // The language this name is in.
  @SerialName("language") val language: NamedApiResource,

  // The version group that uses this flavor text.
  @SerialName("version_group") val versionGroup: NamedApiResource

)

@Serializable data class MoveMetaDataDTO(

  // The status ailment this move inflicts on its target.
  @SerialName("ailment") val ailment: NamedApiResource,

  // The category of move this move falls under, e.g. damage or ailment.
  @SerialName("category") val category: NamedApiResource,

  // The minimum number of times this move hits. Null if it always only hits once.
  @SerialName("min_hits") val minHits: Int?,

  // The maximum number of times this move hits. Null if it always only hits once.
  @SerialName("max_hits") val maxHits: Int?,

  // The minimum number of turns this move continues to take effect. Null if it always only lasts
  // one turn.
  @SerialName("min_turns") val minTurns: Int?,

  // The maximum number of turns this move continues to take effect. Null if it always only lasts
  // one turn.
  @SerialName("max_turns") val maxTurns: Int?,

  // HP drain (if positive) or Recoil damage (if negative), in percent of damage done.
  @SerialName("drain") val drain: Int,

  // The amount of hp gained by the attacking Pokemon, in percent of it's maximum HP.
  @SerialName("healing") val healing: Int,

  // Critical hit rate bonus.
  @SerialName("crit_rate") val critRate: Int,

  // The likelihood this attack will cause an ailment.
  @SerialName("ailment_chance") val ailmentChance: Int,

  // The likelihood this attack will cause the target Pokémon to flinch.
  @SerialName("flinch_chance") val flinchChance: Int,

  // The likelihood this attack will cause a stat change in the target Pokémon.
  @SerialName("stat_chance") val statChance: Int

)

@Serializable data class MoveStatChangeDTO(

  // The amount of change.
  @SerialName("change") val change: Int,

  // The stat being affected.
  @SerialName("stat") val stat: NamedApiResource

)

@Serializable data class PastMoveStatValuesDTO(

  // The percent value of how likely this move is to be successful.
  @SerialName("accuracy") val accuracy: Int?,

  // The percent value of how likely it is this moves effect will take effect.
  @SerialName("effect_chance") val effectChance: Int?,

  // The base power of this move with a value of 0 if it does not have a base power.
  @SerialName("power") val power: Int?,

  // Power points. The number of times this move can be used.
  @SerialName("pp") val pp: Int?,

  // The effect of this move listed in different languages.
  @SerialName("effect_entries") val effectEntries: List<VerboseEffect>,

  // The elemental type of this move.
  @SerialName("type") val type: NamedApiResource?,

  // The version group in which these move stat values were in effect.
  @SerialName("version_group") val versionGroup: NamedApiResource

)

// Move Ailments are status conditions caused by moves used during battle. See
// [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/http://bulbapedia.bulbagarden.net/wiki/Status_condition)
// for greater detail.
@Serializable data class MoveAilmentDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // A list of moves that cause this ailment.
  @SerialName("moves") val moves: List<NamedApiResource>,

  // The name of this resource listed in different languages.
  @SerialName("names") val names: List<NameDTO>

)

@Serializable data class MoveBattleStyleDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // The name of this resource listed in different languages.
  @SerialName("names") val names: List<NameDTO>

)

// Very general categories that loosely group move effects.
@Serializable data class MoveCategoryDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // A list of moves that fall into this category.
  @SerialName("moves") val moves: List<NamedApiResource>,

  // The description of this resource listed in different languages.
  @SerialName("descriptions") val descriptions: List<DescriptionDTO>

)

@Serializable data class MoveDamageClassDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // The description of this resource listed in different languages.
  @SerialName("descriptions") val descriptions: List<DescriptionDTO>,

  // A list of moves that fall into this damage class.
  @SerialName("moves") val moves: List<NamedApiResource>,

  // The name of this resource listed in different languages.
  @SerialName("names") val names: List<NameDTO>

)

// Methods by which Pokémon can learn moves.
@Serializable data class MoveLearnMethodDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // The description of this resource listed in different languages.
  @SerialName("descriptions") val descriptions: List<DescriptionDTO>,

  // The name of this resource listed in different languages.
  @SerialName("names") val names: List<NameDTO>,

  // A list of version groups where moves can be learned through this method.
  @SerialName("version_groups") val versionGroups: List<NamedApiResource>

)

// Targets moves can be directed at during battle. Targets can be Pokémon, environments or even
// other moves.
@Serializable data class MoveTargetDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // The description of this resource listed in different languages.
  @SerialName("descriptions") val descriptions: List<DescriptionDTO>,

  // A list of moves that that are directed at this target.
  @SerialName("moves") val moves: List<NamedApiResource>,

  // The name of this resource listed in different languages.
  @SerialName("names") val names: List<NameDTO>
)