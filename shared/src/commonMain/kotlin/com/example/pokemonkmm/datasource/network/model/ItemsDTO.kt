package com.example.pokemonkmm.datasource.network.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

// An item is an object in the games which the player can pick up, keep in their bag, and use in
// some manner. They have various uses, including healing, powering up, helping catch Pokémon, or to
// access a new area.
@Serializable data class ItemDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // The price of this item in stores.
  @SerialName("cost") val cost: Int,

  // The price of this item in stores.
  @SerialName("fling_power") val flingPower: Int?,

  // The effect of the move Fling when used with this item.
  @SerialName("fling_effect") val flingEffect: NamedApiResource?,

  // A list of attributes this item has.
  @SerialName("attributes") val attributes: List<NamedApiResource>,

  // The category of items this item falls into.
  @SerialName("category") val category: NamedApiResource,

  // The effect of this ability listed in different languages.
  @SerialName("effect_entries") val effectEntries: List<VerboseEffect>,

  // The effect of this ability listed in different languages.
  @SerialName("flavor_text_entries") val flavorTextEntries: List<VersionGroupFlavorTextDTO>,

  // A list of game indices relevent to this item by generation.
  @SerialName("game_indices") val gameIndices: List<GenerationGameIndexDTO>,

  // The name of this item listed in different languages.
  @SerialName("names") val names: List<NameDTO>,

  // A set of sprites used to depict this item in the game.
  @SerialName("sprites") val sprites: ItemSpritesDTO,

  // A list of Pokémon that might be found in the wild holding this item.
  @SerialName("held_by_pokemon") val heldByPokemon: List<ItemHolderPokemonDTO>,

  // A list of Pokémon that might be found in the wild holding this item.
  @SerialName("baby_trigger_for") val babyTriggerFor: ApiResource?,

  // A list of the machines related to this item.
  @SerialName("machines") val machines: List<MachineVersionDetailDTO>

)

@Serializable data class ItemSpritesDTO(

  // The default depiction of this item.
  @SerialName("default") val default: String?

)

@Serializable data class ItemHolderPokemonDTO(
  
  // The Pokémon that holds this item.
  @SerialName("pokemon") val pokemon: NamedApiResource,
  
  // The details for the version that this item is held in by the Pokémon.
  @SerialName("version_details") val versionDetails: List<ItemHolderPokemonVersionDetailDTO>
  
)

@Serializable data class ItemHolderPokemonVersionDetailDTO(

  // How often this Pokémon holds this item in this version.
  @SerialName("rarity") val rarity: Int,

  // The version that this item is held in by the Pokémon.
  @SerialName("version") val version: NamedApiResource

)

// Item attributes define particular aspects of items, e.g. "usable in battle" or "consumable".
@Serializable data class ItemAttributeDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // A list of items that have this attribute.
  @SerialName("items") val items: List<NamedApiResource>,

  // The name of this item attribute listed in different languages.
  @SerialName("names") val names: List<NameDTO>,

  // The description of this item attribute listed in different languages.
  @SerialName("descriptions") val descriptions: List<DescriptionDTO>

)

// Item categories determine where items will be placed in the players bag.
@Serializable data class ItemCategoryDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // A list of items that are a part of this category.
  @SerialName("items") val items: List<NamedApiResource>,

  // The name of this item category listed in different languages.
  @SerialName("names") val names: List<NameDTO>,

  // The pocket items in this category would be put in.
  @SerialName("pocket") val pocket: NamedApiResource

)

// The various effects of the move "Fling" when used with different items.
@Serializable data class ItemFlingEffectDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // The result of this fling effect listed in different languages.
  @SerialName("effect_entries") val effectEntries: List<EffectDTO>,

  // A list of items that have this fling effect.
  @SerialName("items") val items: List<NamedApiResource>

)

// Pockets within the players bag used for storing items by category.
@Serializable data class ItemPocketDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // A list of item categories that are relevant to this item pocket.
  @SerialName("categories") val categories: List<NamedApiResource>,

  // The name of this resource listed in different languages.
  @SerialName("names") val names: List<NameDTO>

)