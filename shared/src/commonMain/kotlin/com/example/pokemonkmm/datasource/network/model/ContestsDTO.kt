package com.example.pokemonkmm.datasource.network.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

// Contest types are categories judges used to weigh a Pokémon's condition in Pokémon contests.
// Check out [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Contest_condition) for greater
// detail.
@Serializable data class ContestTypeDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // The berry flavor that correlates with this contest type.
  @SerialName("berry_flavor") val berryFlavor: NamedApiResource,

  // The name of this contest type listed in different languages.
  @SerialName("names") val names: List<ContestNameDTO>

)

@Serializable data class ContestNameDTO(

  // The name for this contest.
  @SerialName("name") val name: String,

  // The color associated with this contest's name.
  @SerialName("color") val color: String,

  // The language that this name is in.
  @SerialName("language") val language: NamedApiResource

)

// Contest effects refer to the effects of moves when used in contests.
@Serializable data class ContestEffectDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The base number of hearts the user of this move gets.
  @SerialName("appeal") val appeal: Int,

  // The base number of hearts the user's opponent loses.
  @SerialName("jam") val jam: Int,

  // The result of this contest effect listed in different languages.
  @SerialName("effect_entries") val effectEntries: List<EffectDTO>,

  // The flavor text of this contest effect listed in different languages.
  @SerialName("flavor_text_entries") val flavorTextEntries: List<FlavorTextDTO>

)

// Super contest effects refer to the effects of moves when used in super contests.
@Serializable data class SuperContestEffectDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The level of appeal this super contest effect has.
  @SerialName("appeal") val appeal: Int,

  // The flavor text of this super contest effect listed in different languages.
  @SerialName("flavor_text_entries") val flavorTextEntries: List<FlavorTextDTO>,

  // A list of moves that have the effect when used in super contests.
  @SerialName("moves") val moves: List<NamedApiResource>

)