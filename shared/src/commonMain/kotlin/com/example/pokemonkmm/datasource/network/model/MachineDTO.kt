package com.example.pokemonkmm.datasource.network.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


// Machines are the representation of items that teach moves to Pokémon. They vary from version to
// version, so it is not certain that one specific TM or HM corresponds to a single Machine.
@Serializable data class MachineDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The TM or HM item that corresponds to this machine.
  @SerialName("item") val item: NamedApiResource,

  // The move that is taught by this machine.
  @SerialName("move") val move: NamedApiResource,

  // The version group that this machine applies to.
  @SerialName("version_group") val versionGroup: NamedApiResource

)