package com.example.pokemonkmm.datasource.network.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


// Berries are small fruits that can provide HP and status condition restoration, stat enhancement,
// and even damage negation when eaten by Pokémon. Check out
// [Bulbapedia](https://bulbapedia.bulbagarden.net/wiki/Berry) for greater detail.
@Serializable data class BerryDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // Time it takes the tree to grow one stage, in hours. Berry trees go through four of these growth
  // stages before they can be picked.
  @SerialName("growth_time") val growthTime: Int,

  // The maximum number of these berries that can grow on one tree in Generation IV.
  @SerialName("max_harvest") val maxHarvest: Int,

  // The power of the move "Natural Gift" when used with this Berry.
  @SerialName("natural_gift_power") val naturalGiftPower: Int,

  // The size of this Berry, in millimeters.
  @SerialName("size") val size: Int,

  // The smoothness of this Berry, used in making Pokéblocks or Poffins.
  @SerialName("smoothness") val smoothness: Int,

  // The speed at which this Berry dries out the soil as it grows. A higher rate means the soil
  // dries more quickly.
  @SerialName("soil_dryness") val soilDryness: Int,

  // The firmness of this berry, used in making Pokéblocks or Poffins.
  @SerialName("firmness") val firmness: NamedApiResource,

  // A list of references to each flavor a berry can have and the potency of each of those flavors
  // in regard to this berry.
  @SerialName("flavors") val flavors: List<BerryFlavorMapDTO>,

  // Berries are actually items. This is a reference to the item specific data for this berry.
  @SerialName("item") val item: NamedApiResource,

  // The type inherited by "Natural Gift" when used with this Berry.
  @SerialName("natural_gift_type") val naturalGiftType: NamedApiResource

)

@Serializable data class BerryFlavorMapDTO(

  // How powerful the referenced flavor is for this berry.
  @SerialName("potency") val potency: Int,

  // The referenced berry flavor.
  @SerialName("flavor") val flavor: NamedApiResource

)

// Berries can be soft or hard. Check out
// [Bulbapedia](https://bulbapedia.bulbagarden.net/wiki/Category:Berries_by_firmness) for greater
// detail.
@Serializable data class BerryFirmnessDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // A list of the berries with this firmness.
  @SerialName("berries") val berries: List<NamedApiResource>,

  // The name of this resource listed in different languages.
  @SerialName("names") val names: List<NameDTO>

)

// Flavors determine whether a Pokémon will benefit or suffer from eating a berry based on their
// [nature](https://pokeapi.co/docs/v2#natures). Check out
// [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Flavor) for greater detail.
@Serializable data class BerryFlavorDTO(

  // The identifier for this resource.
  @SerialName("id") val id: Int,

  // The name for this resource.
  @SerialName("name") val name: String,

  // A list of the berries with this flavor.
  @SerialName("berries") val berries: List<FlavorBerryMapDTO>,

  // The contest type that correlates with this berry flavor.
  @SerialName("contest_type") val contestType: NamedApiResource,

  // The name of this resource listed in different languages.
  @SerialName("names") val names: List<NameDTO>

)

@Serializable data class FlavorBerryMapDTO (

  // How powerful the referenced flavor is for this berry.
  @SerialName("potency") val potency: Int,

  // The berry with the referenced flavor.
  @SerialName("berry") val berry: NamedApiResource

)