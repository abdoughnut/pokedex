package com.example.pokemonkmm.domain.model


// Machines are the representation of items that teach moves to Pokémon. They vary from version to
// version, so it is not certain that one specific TM or HM corresponds to a single Machine.
data class Machine(

  // The identifier for this resource.
  val id: Int,

  // The TM or HM item that corresponds to this machine.
  val item: Item,

  // The move that is taught by this machine.
  val move: Move,

  // The version group that this machine applies to.
  val versionGroup: VersionGroup

)