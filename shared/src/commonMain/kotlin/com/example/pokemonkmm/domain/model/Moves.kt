package com.example.pokemonkmm.domain.model

// Moves are the skills of Pokémon in battle. In battle, a Pokémon uses one move each turn. Some
// moves (including those learned by Hidden Machine) can be used outside of battle as well, usually
// for the purpose of removing obstacles or exploring new areas.
data class Move(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // The percent value of how likely this move is to be successful.
  val accuracy: Int?,

  // The percent value of how likely it is this moves effect will happen.
  val effectChance: Int?,

  // Power points. The number of times this move can be used.
  val pp: Int?,

  // A value between -8 and 8. Sets the order in which moves are executed during battle. See
  // [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Priority) for greater detail.
  val priority: Int,

  // The base power of this move with a value of 0 if it does not have a base power.
  val power: Int?,

  // A detail of normal and super contest combos that require this move.
  val contestCombos: ContestComboSets?,

  // The type of appeal this move gives a Pokémon when used in a contest.
  val contestType: ContestType?,

  // The effect the move has when used in a contest.
  val contestEffect: ContestEffect?,

  // The type of damage the move inflicts on the target, e.g. physical.
  val damageClass: MoveDamageClass,

  // The effect of this move listed in different languages.
  val effectEntries: List<VerboseEffect>,

  // The list of previous effects this move has had across version groups of the games.
  val effectChanges: List<AbilityEffectChange>,

  // The flavor text of this move listed in different languages.
  val flavorTextEntries: List<MoveFlavorText>,

  // The generation in which this move was introduced.
  val generation: Generation,

  // A list of the machines that teach this move.
  val machines: List<MachineVersionDetail>,

  // Metadata about this move
  val meta: MoveMetaData?,

  // The name of this resource listed in different languages.
  val names: List<Name>,

  // A list of move resource value changes across version groups of the game.
  val pastValues: List<PastMoveStatValues>,

  // A list of stats this moves effects and how much it effects them.
  val statChanges: List<MoveStatChange>,

  // The effect the move has when used in a super contest.
  val superContestEffect: SuperContestEffect?,

  // The type of target that will receive the effects of the attack.
  val target: MoveTarget,

  // The elemental type of this move.
  val type: Type

)

data class ContestComboSets(

  // A detail of moves this move can be used before or after, granting additional appeal points in
  // contests.
  val normalSet: ContestComboDetail,

  // A detail of moves this move can be used before or after, granting additional appeal points in
  // super contests.
  val superSet: ContestComboDetail

)

data class ContestComboDetail(

  // A list of moves to use before this move.
  val useBefore: List<Move>?,

  // A list of moves to use after this move.
  val useAfter: List<Move>?

)

data class MoveFlavorText(

  // The localized flavor text for an api resource in a specific language.
  val flavorText: String,

  // The language this name is in.
  val language: Language,

  // The version group that uses this flavor text.
  val versionGroup: VersionGroup

)

data class MoveMetaData(

  // The status ailment this move inflicts on its target.
  val ailment: MoveAilment,

  // The category of move this move falls under, e.g. damage or ailment.
  val category: MoveCategory,

  // The minimum number of times this move hits. Null if it always only hits once.
  val minHits: Int?,

  // The maximum number of times this move hits. Null if it always only hits once.
  val maxHits: Int?,

  // The minimum number of turns this move continues to take effect. Null if it always only lasts
  // one turn.
  val minTurns: Int?,

  // The maximum number of turns this move continues to take effect. Null if it always only lasts
  // one turn.
  val maxTurns: Int?,

  // HP drain (if positive) or Recoil damage (if negative), in percent of damage done.
  val drain: Int,

  // The amount of hp gained by the attacking Pokemon, in percent of it's maximum HP.
  val healing: Int,

  // Critical hit rate bonus.
  val critRate: Int,

  // The likelihood this attack will cause an ailment.
  val ailmentChance: Int,

  // The likelihood this attack will cause the target Pokémon to flinch.
  val flinchChance: Int,

  // The likelihood this attack will cause a stat change in the target Pokémon.
  val statChance: Int

)

data class MoveStatChange(

  // The amount of change.
  val change: Int,

  // The stat being affected.
  val stat: Stat

)

data class PastMoveStatValues(

  // The percent value of how likely this move is to be successful.
  val accuracy: Int?,

  // The percent value of how likely it is this moves effect will take effect.
  val effectChance: Int?,

  // The base power of this move with a value of 0 if it does not have a base power.
  val power: Int?,

  // Power points. The number of times this move can be used.
  val pp: Int?,

  // The effect of this move listed in different languages.
  val effectEntries: List<VerboseEffect>,

  // The elemental type of this move.
  val type: Type?,

  // The version group in which these move stat values were in effect.
  val versionGroup: VersionGroup

)

// Move Ailments are status conditions caused by moves used during battle. See
// [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/http://bulbapedia.bulbagarden.net/wiki/Status_condition)
// for greater detail.
data class MoveAilment(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // A list of moves that cause this ailment.
  val moves: List<Move>,

  // The name of this resource listed in different languages.
  val names: List<Name>

)

data class MoveBattleStyle(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // The name of this resource listed in different languages.
  val names: List<Name>

)

// Very general categories that loosely group move effects.
data class MoveCategory(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // A list of moves that fall into this category.
  val moves: List<Move>,

  // The description of this resource listed in different languages.
  val descriptions: List<Description>

)

data class MoveDamageClass(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // The description of this resource listed in different languages.
  val descriptions: List<Description>,

  // A list of moves that fall into this damage class.
  val moves: List<Move>,

  // The name of this resource listed in different languages.
  val names: List<Name>

)

// Methods by which Pokémon can learn moves.
data class MoveLearnMethod(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // The description of this resource listed in different languages.
  val descriptions: List<Description>,

  // The name of this resource listed in different languages.
  val names: List<Name>,

  // A list of version groups where moves can be learned through this method.
  val versionGroups: List<VersionGroup>

)

// Targets moves can be directed at during battle. Targets can be Pokémon, environments or even
// other moves.
data class MoveTarget(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // The description of this resource listed in different languages.
  val descriptions: List<Description>,

  // A list of moves that that are directed at this target.
  val moves: List<Move>,

  // The name of this resource listed in different languages.
  val names: List<Name>
)