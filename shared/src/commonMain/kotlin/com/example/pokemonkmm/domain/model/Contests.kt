package com.example.pokemonkmm.domain.model

// Contest types are categories judges used to weigh a Pokémon's condition in Pokémon contests.
// Check out [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Contest_condition) for greater
// detail.
data class ContestType(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // The berry flavor that correlates with this contest type.
  val berryFlavor: BerryFlavor,

  // The name of this contest type listed in different languages.
  val names: List<ContestName>

)

data class ContestName(

  // The name for this contest.
  val name: String,

  // The color associated with this contest's name.
  val color: String,

  // The language that this name is in.
  val language: Language

)

// Contest effects refer to the effects of moves when used in contests.
data class ContestEffect(

  // The identifier for this resource.
  val id: Int,

  // The base number of hearts the user of this move gets.
  val appeal: Int,

  // The base number of hearts the user's opponent loses.
  val jam: Int,

  // The result of this contest effect listed in different languages.
  val effectEntries: List<Effect>,

  // The flavor text of this contest effect listed in different languages.
  val flavorTextEntries: List<FlavorText>

)

// Super contest effects refer to the effects of moves when used in super contests.
data class SuperContestEffect(

  // The identifier for this resource.
  val id: Int,

  // The level of appeal this super contest effect has.
  val appeal: Int,

  // The flavor text of this super contest effect listed in different languages.
  val flavorTextEntries: List<FlavorText>,

  // A list of moves that have the effect when used in super contests.
  val moves: List<Move>

)