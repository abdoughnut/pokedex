package com.example.pokemonkmm.domain.model

// Evolution chains are essentially family trees. They start with the lowest stage within a family
// and detail evolution conditions for each as well as Pokémon they can evolve into up through the
// hierarchy.
data class EvolutionChain(

  // The identifier for this resource.
  val id: Int,

  // The item that a Pokémon would be holding when mating that would trigger the egg hatching a baby
  // Pokémon rather than a basic Pokémon.
  val babyTriggerItem: Item?,

  // The base chain link object. Each link contains evolution details for a Pokémon in the chain.
  // Each link references the next Pokémon in the natural evolution order.
  val chain: ChainLink

)

data class ChainLink(

  // Whether or not this link is for a baby Pokémon. This would only ever be true on the base link.
  val isBaby: Boolean,

  // The Pokémon species at this point in the evolution chain.
  val species: PokemonSpecies,

  // All details regarding the specific details of the referenced Pokémon species evolution.
  val evolutionDetails: List<EvolutionDetail>,

  // A List of chain objects.
  val evolvesTo: List<ChainLink>

)

data class EvolutionDetail(
  // The item required to cause evolution this into Pokémon species.
  val item: Item? = null,

  // The type of event that triggers evolution into this Pokémon species.
  val trigger: EvolutionTrigger,

  // The id of the gender of the evolving Pokémon species must be in order to evolve into this
  // Pokémon species.
  val gender: Int? = null,

  // The item the evolving Pokémon species must be holding during the evolution trigger event to
  // evolve into this Pokémon species.
  val heldItem: Item? = null,

  // The move that must be known by the evolving Pokémon species during the evolution trigger event
  // in order to evolve into this Pokémon species.
  val knownMove: Move? = null,

  // The evolving Pokémon species must know a move with this type during the evolution trigger event
  // in order to evolve into this Pokémon species.
  val knownMoveType: Type? = null,

  // The location the evolution must be triggered at.
  val location: Location? = null,

  // The minimum required level of the evolving Pokémon species to evolve into this Pokémon species.
  val minLevel: Int? = null,

  // The minimum required level of happiness the evolving Pokémon species to evolve into this
  // Pokémon species.
  val minHappiness: Int? = null,

  // The minimum required level of beauty the evolving Pokémon species to evolve into this Pokémon
  // species
  val minBeauty: Int? = null,

  // The minimum required level of affection the evolving Pokémon species to evolve into this
  // Pokémon species.
  val minAffection: Int? = null,

  // Whether or not it must be raining in the overworld to cause evolution this Pokémon species.
  val needsOverworldRain: Boolean = false,

  // The Pokémon species that must be in the players party in order for the evolving Pokémon species
  // to evolve into this Pokémon species.
  val partySpecies: PokemonSpecies? = null,

  // The player must have a Pokémon of this type in their party during the evolution trigger event
  // in order for the evolving Pokémon species to evolve into this Pokémon species.
  val partyType: Type? = null,

  // The required relation between the Pokémon's Attack and Defense stats.
  // 1 means Attack > Defense. 0 means Attack = Defense. -1 means Attack < Defense.
  val relativePhysicalStats: Int? = null,

  // The required time of day. Day or night.
  val timeOfDay: String = "",

  // Pokémon species for which this one must be traded.
  val tradeSpecies: PokemonSpecies? = null,

  // Whether or not the 3DS needs to be turned upside-down as this Pokémon levels up.
  val turnUpsideDown: Boolean = false

)

// Evolution triggers are the events and conditions that cause a Pokémon to evolve. Check out
// [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Methods_of_evolution) for greater detail.
data class EvolutionTrigger(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // The name of this resource listed in different languages.
  val names: List<Name>,

  // A list of pokemon species that result from this evolution trigger.
  val pokemonSpecies: List<PokemonSpecies>

)