package com.example.pokemonkmm.domain.model

// An item is an object in the games which the player can pick up, keep in their bag, and use in
// some manner. They have various uses, including healing, powering up, helping catch Pokémon, or to
// access a new area.
data class Item(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // The price of this item in stores.
  val cost: Int,

  // The price of this item in stores.
  val flingPower: Int?,

  // The effect of the move Fling when used with this item.
  val flingEffect: ItemFlingEffect?,

  // A list of attributes this item has.
  val attributes: List<ItemAttribute>,

  // The category of items this item falls into.
  val category: ItemCategory,

  // The effect of this ability listed in different languages.
  val effectEntries: List<VerboseEffect>,

  // The effect of this ability listed in different languages.
  val flavorTextEntries: List<VersionGroupFlavorText>,

  // A list of game indices relevant to this item by generation.
  val gameIndices: List<GenerationGameIndex>,

  // The name of this item listed in different languages.
  val names: List<Name>,

  // A set of sprites used to depict this item in the game.
  val sprites: ItemSprites,

  // A list of Pokémon that might be found in the wild holding this item.
  val heldByPokemon: List<ItemHolderPokemon>,

  // A list of Pokémon that might be found in the wild holding this item.
  val babyTriggerFor: EvolutionChain?,

  // A list of the machines related to this item.
  val machines: List<MachineVersionDetail>

)

data class ItemSprites(

  // The default depiction of this item.
  val default: String?

)

data class ItemHolderPokemon(

  // The Pokémon that holds this item.
  val pokemon: Pokemon,

  // The details for the version that this item is held in by the Pokémon.
  val versionDetails: List<ItemHolderPokemonVersionDetail>

)

data class ItemHolderPokemonVersionDetail(

  // How often this Pokémon holds this item in this version.
  val rarity: Int,

  // The version that this item is held in by the Pokémon.
  val version: Version

)

// Item attributes define particular aspects of items, e.g. "usable in battle" or "consumable".
data class ItemAttribute(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // A list of items that have this attribute.
  val items: List<Item>,

  // The name of this item attribute listed in different languages.
  val names: List<Name>,

  // The description of this item attribute listed in different languages.
  val descriptions: List<Description>

)

// Item categories determine where items will be placed in the players bag.
data class ItemCategory(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // A list of items that are a part of this category.
  val items: List<Item>,

  // The name of this item category listed in different languages.
  val names: List<Name>,

  // The pocket items in this category would be put in.
  val pocket: ItemPocket

)

// The various effects of the move "Fling" when used with different items.
data class ItemFlingEffect(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // The result of this fling effect listed in different languages.
  val effectEntries: List<Effect>,

  // A list of items that have this fling effect.
  val items: List<Item>

)

// Pockets within the players bag used for storing items by category.
data class ItemPocket(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // A list of item categories that are relevant to this item pocket.
  val categories: List<ItemCategory>,

  // The name of this resource listed in different languages.
  val names: List<Name>

)