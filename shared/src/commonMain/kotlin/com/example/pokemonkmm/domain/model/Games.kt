package com.example.pokemonkmm.domain.model


// A generation is a grouping of the Pokémon games that separates them based on the Pokémon they
// include. In each generation, a new set of Pokémon, Moves, Abilities and Types that did not exist
// in the previous generation are released.
data class Generation(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // A list of abilities that were introduced in this generation.
  val abilities: List<Ability>,

  // The name of this resource listed in different languages.
  val names: List<Name>,

  // The main region travelled in this generation.
  val mainRegion: Region,

  // A list of moves that were introduced in this generation.
  val moves: List<Move>,

  // A list of Pokémon species that were introduced in this generation.
  val pokemonSpecies: List<PokemonSpecies>,

  // A list of types that were introduced in this generation.
  val types: List<Type>,

  // A list of version groups that were introduced in this generation.
  val versionGroups: List<VersionGroup>

)

// A Pokédex is a handheld electronic encyclopedia device; one which is capable of recording and
// retaining information of the various Pokémon in a given region with the exception of the national
// dex and some smaller dexes related to portions of a region.
// See [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Pokedex) for greater detail.
data class Pokedex(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // Whether or not this Pokédex originated in the main series of the video games.
  val isMainSeries: Boolean,

  // The description of this resource listed in different languages.
  val descriptions: List<Description>,

  // The name of this resource listed in different languages.
  val names: List<Name>,

  // A list of Pokémon catalogued in this Pokédex and their indexes.
  val pokemonEntries: List<PokemonEntry>,

  // The region this Pokédex catalogues Pokémon for.
  val region: Region?,

  // A list of version groups this Pokédex is relevant to.
  val versionGroups: List<VersionGroup>

)

data class PokemonEntry(

  // The index of this Pokémon species entry within the Pokédex.
  val entryNumber: Int,

  // The Pokémon species being encountered.
  val pokemonSpecies: PokemonSpecies

)

// Versions of the games, e.g., Red, Blue or Yellow.
data class Version(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // The name of this resource listed in different languages.
  val names: List<Name>,

  // The version group this version belongs to.
  val versionGroup: VersionGroup

)

// Version groups categorize highly similar versions of the games.
data class VersionGroup(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // Order for sorting. Almost by date of release, except similar versions are grouped together.
  val order: Int,

  // The generation this version was introduced in.
  val generation: Generation,

  // A list of methods in which Pokémon can learn moves in this version group.
  val moveLearnMethods: List<MoveLearnMethod>,

  // A list of Pokédexes introduces in this version group.
  val pokedexes: List<Pokedex>,

  // A list of Pokédexes introduces in this version group.
  val regions: List<Region>,

  // The versions this version group owns.
  val versions: List<Version>

)