package com.example.pokemonkmm.domain.model

// Methods by which the player might can encounter Pokémon in the wild, e.g., walking in tall grass.
// Check out [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Wild_Pok%C3%A9mon) for greater detail.
data class EncounterMethod(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // A good value for sorting.
  val order: Int,

  // The name of this resource listed in different languages.
  val names: List<Name>

)

// Conditions which affect what pokemon might appear in the wild, e.g., day or night.
data class EncounterCondition(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // The name of this resource listed in different languages.
  val names: List<Name>,

  // A  list of possible values for this encounter condition.
  val values: List<EncounterConditionValue>

)

data class EncounterConditionValue(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // The condition this encounter condition value pertains to
  val condition: EncounterCondition,

  // The name of this resource listed in different languages.
  val names: List<Name>

)