package com.example.pokemonkmm.domain.model


// Languages for translations of API resource information.
data class Language(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // Whether or not the games are published in this language.
  val official: Boolean,

  // The two-letter code of the country where this language is spoken. Note that it is not unique.
  val iso639: String,

  // The two-letter code of the language. Note that it is not unique.
  val iso3166: String,

  // The name of this resource listed in different languages.
  val names: List<Name>

)

data class Description(

  // The localized description for an API resource in a specific language.
  val description: String,

  // The language this name is in.
  val language: Language

)

data class Effect(

  // The localized effect text for an API resource in a specific language.
  val effect: String,

  // The language this effect is in.
  val language: Language

)

data class Encounter(

  // The lowest level the Pokémon could be encountered at.
  val minLevel: Int,

  // The highest level the Pokémon could be encountered at.
  val maxLevel: Int,

  // A list of condition values that must be in effect for this encounter to occur.
  val conditionValues: List<EncounterConditionValue>,

  // Percent chance that this encounter will occur.
  val chance: Int,

  // The method by which this encounter happens.
  val method: EncounterMethod

)

data class FlavorText(

  // The localized flavor text for an API resource in a specific language.
  val flavorText: String,

  // The language this name is in.
  val language: Language,

  // The game version this flavor text is extracted from.
  val version: Version

)

data class GenerationGameIndex(

  // The internal id of an API resource within game data.
  val gameIndex: Int,

  // The generation relevant to this game index.
  val generation: Generation

)

data class MachineVersionDetail(

  // The machine that teaches a move from an item.
  val machine: Machine,

  // The version group of this specific machine.
  val versionGroup: VersionGroup

)

data class Name(

  // The localized name for an API resource in a specific language.
  val name: String,

  // The language this name is in.
  val language: Language

)

data class VerboseEffect(

  // The localized effect text for an API resource in a specific language.
  val effect: String,

  // The localized effect text in brief.
  val shortEffect: String,

  // The language this effect is in.
  val language: Language

)

data class VersionEncounterDetail(

  // The game version this encounter happens in.
  val version: Version,

  // The total percentage of all encounter potential.
  val maxChance: Int,

  // A list of encounters and their specifics.
  val encounterDetails: List<Encounter>

)

data class VersionGameIndex(

  // The internal id of an API resource within game data.
  val gameIndex: Int,

  // The version relevant to this game index.
  val version: Version

)

data class VersionGroupFlavorText(

  // The localized name for an API resource in a specific language.
  val text: String,

  // The language this name is in.
  val language: Language,

  // The version group which uses this flavor text.
  val versionGroup: VersionGroup

)