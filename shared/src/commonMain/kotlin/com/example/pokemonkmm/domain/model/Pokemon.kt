package com.example.pokemonkmm.domain.model

import kotlinx.serialization.SerialName


// Abilities provide passive effects for Pokémon in battle or in the overworld. Pokémon have
// multiple possible abilities but can have only one ability at a time. Check out
// [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Ability) for greater detail.
data class Ability(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // Whether or not this ability originated in the main series of the video games.
  val isMainSeries: Boolean,

  // The generation this ability originated in.
  val generation: Generation,

  // The name of this resource listed in different languages.
  val names: List<Name>,

  // The effect of this ability listed in different languages.
  val effectEntries: List<VerboseEffect>,

  // The list of previous effects this ability has had across version groups.
  val effectChanges: List<AbilityEffectChange>,

  // The flavor text of this ability listed in different languages.
  val flavorTextEntries: List<AbilityFlavorText>,

  // A list of Pokémon that could potentially have this ability.
  val pokemon: List<AbilityPokemon>

)

data class AbilityEffectChange(

  // The previous effect of this ability listed in different languages.
  val effectEntries: List<Effect>,

  // The version group in which the previous effect of this ability originated.
  val versionGroup: VersionGroup

)

data class AbilityFlavorText(

  // The localized name for an API resource in a specific language.
  val flavorText: String,

  // The language this text resource is in.
  val language: Language,

  // The version group that uses this flavor text.
  val versionGroup: VersionGroup

)

data class AbilityPokemon(

  // Whether or not this a hidden ability for the referenced Pokémon.
  val isHidden: Boolean,

  // Pokémon have 3 ability 'slots' which hold references to possible abilities they could have.
  // This is the slot of this ability for the referenced pokemon.
  val slot: Int,

  // The Pokémon this ability could belong to.
  val pokemon: Pokemon

)

// Characteristics indicate which stat contains a Pokémon's highest IV. A Pokémon's Characteristic
// is determined by the remainder of its highest IV divided by 5 (gene_modulo).
// Check out [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Characteristic) for greater detail.
data class Characteristic(

  // The identifier for this resource.
  val id: Int,

  // The remainder of the highest stat/IV divided by 5.
  val geneModulo: Int,

  // The possible values of the highest stat that would result in a Pokémon receiving this
  // characteristic when divided by 5.
  val possibleValues: List<Int>

)

// Egg Groups are categories which determine which Pokémon are able to interbreed. Pokémon may
// belong to either one or two Egg Groups.
// Check out [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Egg_Group) for greater detail.
data class EggGroup(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // The name of this resource listed in different languages.
  val names: List<Name>,

  // A list of all Pokémon species that are members of this egg group.
  val pokemonSpecies: List<PokemonSpecies>

)

// Genders were introduced in Generation II for the purposes of breeding Pokémon but can also result
// in visual differences or even different evolutionary lines.
// Check out [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Gender) for greater detail.
data class Gender(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // A list of Pokémon species that can be this gender and how likely it is that they will be.
  val pokemonSpeciesDetails: List<PokemonSpeciesGender>,

  // A list of Pokémon species that required this gender in order for a Pokémon to evolve into them.
  val requiredForEvolution: List<PokemonSpecies>

)

data class PokemonSpeciesGender(

  // The chance of this Pokémon being female, in eighths; or -1 for genderless.
  @SerialName("rate") val rate: Int,

  // A Pokémon species that can be the referenced gender.
  val pokemonSpecies: PokemonSpecies

)

// Growth rates are the speed with which Pokémon gain levels through experience.
// Check out [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Experience) for greater detail.
data class GrowthRate(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // The formula used to calculate the rate at which the Pokémon species gains level.
  val formula: String,

  // The descriptions of this characteristic listed in different languages.
  val descriptions: List<Description>,

  // A list of levels and the amount of experienced needed to atain them based on this growth rate.
  val levels: List<GrowthRateExperienceLevel>,

  // A list of Pokémon species that gain levels at this growth rate.
  val pokemonSpecies: List<PokemonSpecies>

)

data class GrowthRateExperienceLevel(

  // The level gained.
  val level: Int,

  // The amount of experience required to reach the referenced level.
  val experience: Int

)

// Natures influence how a Pokémon's stats grow.
// See [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Nature) for greater detail.
data class Nature(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // The stat decreased by 10% in Pokémon with this nature.
  val decreasedStat: Stat?,

  // The stat increased by 10% in Pokémon with this nature.
  val increasedStat: Stat?,

  // The flavor hated by Pokémon with this nature.
  val hatesFlavor: BerryFlavor?,

  // The flavor liked by Pokémon with this nature.
  val likesFlavor: BerryFlavor?,

  // A list of Pokéathlon stats this nature effects and how much it effects them.
  val pokeathlonStatChanges: List<NatureStatChange>,

  // The name of this resource listed in different languages.
  val moveBattleStylePreferences: List<MoveBattleStylePreference>,

  // The name of this resource listed in different languages.
  val names: List<Name>

)

data class NatureStatChange(

  // The amount of change.
  val maxChange: Int,

  // The stat being affected.
  val pokeathlonStat: PokeathlonStat

)

data class MoveBattleStylePreference(

  // Chance of using the move, in percent, if HP is under one half.
  val lowHpPreference: Int,

  // Chance of using the move, in percent, if HP is over one half.
  val highHpPreference: Int,

  // The move battle style.
  val moveBattleStyle: MoveBattleStyle

)

// Pokéathlon Stats are different attributes of a Pokémon's performance in Pokéathlons. In
// Pokéathlons, competitions happen on different courses; one for each of the different Pokéathlon
// stats. See [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Pok%C3%A9athlon) for greater
// detail.
data class PokeathlonStat(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // The name of this resource listed in different languages.
  val names: List<Name>,

  // A detail of natures which affect this Pokéathlon stat positively or negatively.
  val affectingNatures: NaturePokeathlonStatAffectSets

)

data class NaturePokeathlonStatAffectSets(

  // A list of natures and how they change the referenced Pokéathlon stat.
  val increase: List<NaturePokeathlonStatAffect>,

  // A list of natures and how they change the referenced Pokéathlon stat.
  val decrease: List<NaturePokeathlonStatAffect>

)

data class NaturePokeathlonStatAffect(

  // The maximum amount of change to the referenced Pokéathlon stat.
  val maxChange: Int,

  // The nature causing the change.
  val nature: Nature

)

// Pokémon are the creatures that inhabit the world of the Pokémon games. They can be caught using
// Pokéballs and trained by battling with other Pokémon. Each Pokémon belongs to a specific species
// but may take on a variant which makes it differ from other Pokémon of the same species, such as
// base stats, available abilities and typings.
// See [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Pok%C3%A9mon_(species)) for greater
// detail.
data class Pokemon(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // The base experience gained for defeating this Pokémon.
  val baseExperience: Int,

  // The height of this Pokémon in decimetres.
  val height: Int,

  // Order for sorting. Almost national order, except families are grouped together.
  val order: Int,

  // The weight of this Pokémon in hectograms.
  val weight: Int,

  // A set of sprites used to depict this Pokémon in the game. A visual representation of the
  // various sprites can be found at [PokeAPI/sprites](https://github.com/PokeAPI/sprites#sprites)
  val sprites: PokemonSprites,

)
//data class Pokemon(
//
//  // The identifier for this resource.
//  val id: Int,
//
//  // The name for this resource.
//  val name: String,
//
//  // The base experience gained for defeating this Pokémon.
//  val baseExperience: Int,
//
//  // The height of this Pokémon in decimetres.
//  val height: Int,
//
//  // Set for exactly one Pokémon used as the default for each species.
//  val isDefault: Boolean,
//
//  // Order for sorting. Almost national order, except families are grouped together.
//  val order: Int,
//
//  // The weight of this Pokémon in hectograms.
//  val weight: Int,
//
//  // A list of abilities this Pokémon could potentially have.
//  val abilities: List<PokemonAbility>,
//
//  // A list of forms this Pokémon can take on.
//  val forms: List<PokemonForm>,
//
//  // A list of game indices relevant to Pokémon item by generation.
//  val gameIndices: List<VersionGameIndex>,
//
//  // A list of items this Pokémon may be holding when encountered.
//  val heldItems: List<PokemonHeldItem>,
//
//  // A link to a list of location areas, as well as encounter details pertaining to specific
//  // versions.
//  val locationAreaEncounters: String,
//
//  // A list of moves along with learn methods and level details pertaining to specific version
//  // groups.
//  val moves: List<PokemonMove>,
//
//  // A set of sprites used to depict this Pokémon in the game. A visual representation of the
//  // various sprites can be found at [PokeAPI/sprites](https://github.com/PokeAPI/sprites#sprites)
//  val sprites: PokemonSprites,
//
//  // The species this Pokémon belongs to.
//  val species: PokemonSpecies,
//
//  // A list of base stat values for this Pokémon.
//  val stats: List<PokemonStat>,
//
//  // A list of details showing types this Pokémon has.
//  val types: List<PokemonType>
//
//)

data class PokemonAbility(

  // Whether or not this is a hidden ability.
  val isHidden: Boolean,

  // The slot this ability occupies in this Pokémon species.
  val slot: Int,

  // The ability the Pokémon may have.
  val ability: Ability

)

data class PokemonType(

  // The order the Pokémon's types are listed in.
  val slot: Int,

  // The type the referenced Pokémon has.
  val type: Type

)

data class PokemonHeldItem(

  // The item the referenced Pokémon holds.
  val item: Item,

  // The details of the different versions in which the item is held.
  val versionDetails: List<PokemonHeldItemVersion>

)

data class PokemonHeldItemVersion(

  // The version in which the item is held.
  val version: Version,

  // How often the item is held.
  val rarity: Int

)

data class PokemonMove(

  // The move the Pokémon can learn.
  val move: Move,

  // The details of the version in which the Pokémon can learn the move.
  val versionGroupDetails: List<PokemonMoveVersion>

)

data class PokemonMoveVersion(

  // The method by which the move is learned.
  val moveLearnMethod: MoveLearnMethod,

  // The version group in which the move is learned.
  val versionGroup: VersionGroup,

  // The minimum level to learn the move.
  val levelLearnedAt: Int

)

data class PokemonStat(

  // The stat the Pokémon has.
  @SerialName("stat") val stat: Stat,

  // The effort points (EV) the Pokémon has in the stat.
  @SerialName("effort") val effort: Int,

  // The base value of the stat.
  @SerialName("base_stat") val baseStat: Int

)

data class PokemonSprites(

  // The default depiction of this Pokémon from the back in battle.
  @SerialName("back_default") val backDefault: String?,

  // The shiny depiction of this Pokémon from the back in battle.
  @SerialName("back_shiny") val backShiny: String?,

  // The female depiction of this Pokémon from the back in battle.
  @SerialName("back_female") val backFemale: String?,

  // The shiny female depiction of this Pokémon from the back in battle.
  @SerialName("back_shiny_female") val backShinyFemale: String?,

  // The default depiction of this Pokémon from the front in battle.
  @SerialName("front_default") val frontDefault: String?,

  // The shiny depiction of this Pokémon from the front in battle.
  @SerialName("front_shiny") val frontShiny: String?,

  // The female depiction of this Pokémon from the front in battle.
  @SerialName("front_female") val frontFemale: String?,

  // The shiny female depiction of this Pokémon from the front in battle.
  @SerialName("front_shiny_female") val frontShinyFemale: String?

)

// Pokémon Location Areas are ares where Pokémon can be found.
data class LocationAreaEncounter(

  // The location area the referenced Pokémon can be encountered in.
  @SerialName("location_area") val locationArea: LocationArea,

  // A list of versions and encounters with the referenced Pokémon that might happen.
  val versionDetails: List<VersionEncounterDetail>

)

// Colors used for sorting Pokémon in a Pokédex. The color listed in the Pokédex is usually the
// color most apparent or covering each Pokémon's body. No orange category exists; Pokémon that are
// primarily orange are listed as red or brown.
data class PokemonColor(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // The name of this resource listed in different languages.
  val names: List<Name>,

  // A list of the Pokémon species that have this color.
  val pokemonSpecies: List<PokemonSpecies>

)

// Some Pokémon may appear in one of multiple, visually different forms. These differences are
// purely cosmetic. For variations within a Pokémon species, which do differ in more than just
// visuals, the 'Pokémon' entity is used to represent such a variety.
data class PokemonForm(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // The order in which forms should be sorted within all forms. Multiple forms may have equal
  // order, in which case they should fall back on sorting by name.
  val order: Int,

  // The order in which forms should be sorted within a species' forms.
  @SerialName("form_order") val formOrder: Int,

  // True for exactly one form used as the default for each Pokémon.
  val isDefault: Boolean,

  // Whether or not this form can only happen during battle.
  @SerialName("is_battle_only") val isBattleOnly: Boolean,

  // Whether or not this form requires mega evolution.
  @SerialName("is_mega") val isMega: Boolean,

  // The name of this form.
  @SerialName("form_name") val formName: String,

  // The Pokémon that can take on this form.
  val pokemon: Pokemon,

  // A set of sprites used to depict this Pokémon form in the game.
  val sprites: PokemonFormSprites,

  // The version group this Pokémon form was introduced in.
  val versionGroup: VersionGroup,

  // The form specific form name of this Pokémon form, or empty if the form does not have a specific
  // name.
  @SerialName("form_names") val formNames: List<Name>

)

data class PokemonFormSprites(

  // The default depiction of this Pokémon form from the back in battle.
  @SerialName("back_default") val backDefault: String?,

  // The shiny depiction of this Pokémon form from the back in battle.
  @SerialName("back_shiny") val backShiny: String?,

  // The default depiction of this Pokémon form from the front in battle.
  @SerialName("front_default") val frontDefault: String?,

  // The shiny depiction of this Pokémon form from the front in battle.
  @SerialName("front_shiny") val frontShiny: String?

)

// Habitats are generally different terrain Pokémon can be found in but can also be areas designated
// for rare or legendary Pokémon.
data class PokemonHabitat(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // The name of this resource listed in different languages.
  val names: List<Name>,

  // A list of the Pokémon species that can be found in this habitat.
  val pokemonSpecies: List<PokemonSpecies>

)

// Shapes used for sorting Pokémon in a Pokédex.
data class PokemonShape(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // The "scientific" name of this Pokémon shape listed in different languages.
  @SerialName("awesome_names") val awesomeNames: List<AwesomeName>,

  // The name of this resource listed in different languages.
  val names: List<Name>,

  // A list of the Pokémon species that have this shape.
  @SerialName("pokemonSpecies") val pokemonSpecies: List<PokemonSpecies>

)

data class AwesomeName(

  // The localized "scientific" name for an API resource in a specific language.
  @SerialName("awesome_name") val awesomeName: String,

  // The language this "scientific" name is in.
  val language: Language

)

// A Pokémon Species forms the basis for at least one Pokémon. Attributes of a Pokémon species are
// shared across all varieties of Pokémon within the species. A good example is Wormadam; Wormadam
// is the species which can be found in three different varieties, Wormadam-Trash, Wormadam-Sandy
// and Wormadam-Plant.
data class PokemonSpecies(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // The order in which species should be sorted. Based on National Dex order, except families are
  // grouped together and sorted by stage.
  val order: Int,

  // The chance of this Pokémon being female, in eighths; or -1 for genderless.
  @SerialName("gender_rate") val genderRate: Int,

  // The base capture rate; up to 255. The higher the number, the easier the catch.
  @SerialName("capture_rate") val captureRate: Int,

  // The happiness when caught by a normal Pokéball; up to 255. The higher the number, the happier
  // the Pokémon.
  @SerialName("base_happiness") val baseHappiness: Int,

  // Whether or not this is a baby Pokémon.
  @SerialName("is_baby") val isBaby: Boolean,

  // Whether or not this is a legendary Pokémon.
  @SerialName("is_legendary") val isLegendary: Boolean,

  // Whether or not this is a mythical Pokémon.
  @SerialName("is_mythical") val isMythical: Boolean,

  // Initial hatch counter: one must walk 255 × (hatch_counter + 1) steps before this Pokémon's egg
  // hatches, unless utilizing bonuses like Flame Body's.
  @SerialName("hatch_counter") val hatchCounter: Int,

  // Whether or not this Pokémon has visual gender differences.
  @SerialName("has_gender_differences") val hasGenderDifferences: Boolean,

  // Whether or not this Pokémon has multiple forms and can switch between them.
  @SerialName("forms_switchable") val formsSwitchable: Boolean,

  // The rate at which this Pokémon species gains levels.
  @SerialName("growth_rate") val growthRate: GrowthRate,

  // A list of Pokedexes and the indexes reserved within them for this Pokémon species.
  @SerialName("pokedex_numbers") val pokedexNumbers: List<PokemonSpeciesDexEntry>,

  // A list of egg groups this Pokémon species is a member of.
  @SerialName("egg_groups") val eggGroups: List<EggGroup>,

  // The color of this Pokémon for Pokédex search.
  @SerialName("color") val color: PokemonColor,

  // The shape of this Pokémon for Pokédex search.
  @SerialName("shape") val shape: PokemonShape,

  // The Pokémon species that evolves into this Pokemon_species.
  @SerialName("evolves_from_species") val evolvesFromSpecies: PokemonSpecies?,

  // The evolution chain this Pokémon species is a member of.
  @SerialName("evolution_chain") val evolutionChain: EvolutionChain,

  // The habitat this Pokémon species can be encountered in.
  @SerialName("habitat") val habitat: PokemonHabitat?,

  // The generation this Pokémon species was introduced in.
  val generation: Generation,

  // The name of this resource listed in different languages.
  val names: List<Name>,

  // A list of encounters that can be had with this Pokémon species in pal park.
  @SerialName("pal_park_encounters") val palParkEncounters: List<PalParkEncounterArea>,

  // A list of flavor text entries for this Pokémon species.
  val flavorTextEntries: List<FlavorText>,

  // Descriptions of different forms Pokémon take on within the Pokémon species.
  @SerialName("form_descriptions") val formDescriptions: List<Description>,

  // The genus of this Pokémon species listed in multiple languages.
  @SerialName("genera") val genera: List<Genus>,

  // A list of the Pokémon that exist within this Pokémon species.
  @SerialName("varieties") val varieties: List<PokemonSpeciesVariety>

)

data class Genus(

  // The localized genus for the referenced Pokémon species
  @SerialName("genus") val genus: String,

  // The language this genus is in.
  val language: Language

)

data class PokemonSpeciesDexEntry(

  // The index number within the Pokédex.
  @SerialName("entry_number") val entryNumber: Int,

  // The Pokédex the referenced Pokémon species can be found in.
  @SerialName("pokedex") val pokedex: Pokedex

)

data class PalParkEncounterArea(

  // The base score given to the player when the referenced Pokémon is caught during a pal park run.
  @SerialName("base_score") val baseScore: Int,

  // The base rate for encountering the referenced Pokémon in this pal park area.
  @SerialName("rate") val rate: Int,

  // The pal park area where this encounter happens.
  @SerialName("area") val area: PalParkArea

)

data class PokemonSpeciesVariety(

  // Whether this variety is the default variety.
  val isDefault: Boolean,

  // The Pokémon variety.
  val pokemon: Pokemon

)

// Stats determine certain aspects of battles. Each Pokémon has a value for each stat which grows as
// they gain levels and can be altered momentarily by effects in battles.
data class Stat(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // ID the games use for this stat.
  @SerialName("game_index") val gameIndex: Int,

  // Whether this stat only exists within a battle.
  @SerialName("is_battle_only") val isBattleOnly: Boolean,

  // A detail of moves which affect this stat positively or negatively.
  @SerialName("affecting_moves") val affectingMoves: MoveStatAffectSets,

  // A detail of natures which affect this stat positively or negatively.
  @SerialName("affecting_natures") val affectingNatures: NatureStatAffectSets,

  // A list of characteristics that are set on a Pokémon when its highest base stat is this stat.
  @SerialName("characteristics") val characteristics: List<Characteristic>,

  // The class of damage this stat is directly related to.
  @SerialName("move_damage_class") val moveDamageClass: MoveDamageClass?,

  // The name of this resource listed in different languages.
  val names: List<Name>

)

data class MoveStatAffectSets(

  // A list of natures and how they change the referenced stat.
  val increase: List<MoveStatAffect>,

  // A list of nature sand how they change the referenced stat.
  val decrease: List<MoveStatAffect>

)

data class MoveStatAffect(

  // // The maximum amount of change to the referenced stat.
  @SerialName("change") val change: Int,

  // // The move causing the change.
  val move: Move
)

data class NatureStatAffectSets(

  // A list of natures and how they change the referenced stat.
  val increase: List<Nature>,

  // A list of nature sand how they change the referenced stat.
  val decrease: List<Nature>

)

// Types are properties for Pokémon and their moves. Each type has three properties: which types of
// Pokémon it is super effective against, which types of Pokémon it is not very effective against,
// and which types of Pokémon it is completely ineffective against.
data class Type(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // A detail of how effective this type is toward others and vice versa.
  @SerialName("damage_relations") val damageRelations: TypeRelations,

  // A list of game indices relevant to this item by generation.
  val gameIndices: List<GenerationGameIndex>,

  // The generation this type was introduced in.
  val generation: Generation,

  // The class of damage inflicted by this type.
  @SerialName("move_damage_class") val moveDamageClass: MoveDamageClass?,

  // The name of this resource listed in different languages.
  val names: List<Name>,

  // A list of details of Pokémon that have this type.
  val pokemon: List<TypePokemon>,

  // A list of moves that have this type.
  val moves: List<Move>

)

data class TypePokemon(

  // The order the Pokémon's types are listed in.
  val slot: Int,

  // The Pokémon that has the referenced type.
  val pokemon: Pokemon

)

data class TypeRelations(

  // A list of types this type has no effect on.
  @SerialName("no_damage_to") val noDamageTo: List<Type>,

  // A list of types this type is not very effect against.
  @SerialName("half_damage_to") val halfDamageTo: List<Type>,

  // A list of types this type is very effect against.
  @SerialName("double_damage_to") val doubleDamageTo: List<Type>,

  // A list of types that have no effect on this type.
  @SerialName("no_damage_from") val noDamageFrom: List<Type>,

  // A list of types that are not very effective against this type.
  @SerialName("half_damage_from") val halfDamageFrom: List<Type>,

  // A list of types that are very effective against this type.
  @SerialName("double_damage_from") val doubleDamageFrom: List<Type>

)