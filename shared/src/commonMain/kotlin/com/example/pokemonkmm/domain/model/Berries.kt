package com.example.pokemonkmm.domain.model


// Berries are small fruits that can provide HP and status condition restoration, stat enhancement,
// and even damage negation when eaten by Pokémon. Check out
// [Bulbapedia](https://bulbapedia.bulbagarden.net/wiki/Berry) for greater detail.
data class Berry(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // Time it takes the tree to grow one stage, in hours. Berry trees go through four of these growth
  // stages before they can be picked.
  val growthTime: Int,

  // The maximum number of these berries that can grow on one tree in Generation IV.
  val maxHarvest: Int,

  // The power of the move "Natural Gift" when used with this Berry.
  val naturalGiftPower: Int,

  // The size of this Berry, in millimeters.
  val size: Int,

  // The smoothness of this Berry, used in making Pokéblocks or Poffins.
  val smoothness: Int,

  // The speed at which this Berry dries out the soil as it grows. A higher rate means the soil
  // dries more quickly.
  val soilDryness: Int,

  // The firmness of this berry, used in making Pokéblocks or Poffins.
  val firmness: BerryFirmness,

  // A list of references to each flavor a berry can have and the potency of each of those flavors
  // in regard to this berry.
  val flavors: List<BerryFlavorMap>,

  // Berries are actually items. This is a reference to the item specific data for this berry.
  val item: Item,

  // The type inherited by "Natural Gift" when used with this Berry.
  val naturalGiftType: Type

)

data class BerryFlavorMap(

  // How powerful the referenced flavor is for this berry.
  val potency: Int,

  // The referenced berry flavor.
  val flavor: BerryFlavor

)

// Berries can be soft or hard. Check out
// [Bulbapedia](https://bulbapedia.bulbagarden.net/wiki/Category:Berries_by_firmness) for greater
// detail.
data class BerryFirmness(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // A list of the berries with this firmness.
  val berries: List<Berry>,

  // The name of this resource listed in different languages.
  val names: List<Name>

)

// Flavors determine whether a Pokémon will benefit or suffer from eating a berry based on their
// [nature](https://pokeapi.co/docs/v2#natures). Check out
// [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Flavor) for greater detail.
data class BerryFlavor(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // A list of the berries with this flavor.
  val berries: List<FlavorBerryMap>,

  // The contest type that correlates with this berry flavor.
  val contestType: ContestType,

  // The name of this resource listed in different languages.
  val names: List<Name>

)

data class FlavorBerryMap(

  // How powerful the referenced flavor is for this berry.
  val potency: Int,

  // The berry with the referenced flavor.
  val berry: Berry

)