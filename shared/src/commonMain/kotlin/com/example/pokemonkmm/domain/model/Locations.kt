package com.example.pokemonkmm.domain.model


// Locations that can be visited within the games. Locations make up sizable portions of regions,
// like cities or routes.
data class Location(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // The region this location can be found in.
  val region: Region?,

  // The name of this resource listed in different languages.
  val names: List<Name>,

  // A list of game indices relevant to this location by generation.
  val gameIndices: List<GenerationGameIndex>,

  // Areas that can be found within this location.
  val areas: List<LocationArea>

)

// Location areas are sections of areas, such as floors in a building or cave. Each area has its own
// set of possible Pokémon encounters.
data class LocationArea(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // The internal id of an API resource within game data.
  val gameIndex: Int,

  // A list of methods in which Pokémon may be encountered in this area and how likely the method
  // will occur depending on the version of the game.
  val encounterMethodRates: List<EncounterMethodRate>,

  // The region this location area can be found in.
  val location: Location,

  // The name of this resource listed in different languages.
  val names: List<Name>,

  // A list of Pokémon that can be encountered in this area along with version specific details
  // about the encounter.
  val pokemonEncounters: List<PokemonEncounter>

)

data class EncounterMethodRate(

  // The method in which Pokémon may be encountered in an area..
  val encounterMethod: EncounterMethod,

  // The chance of the encounter to occur on a version of the game.
  val versionDetails: List<EncounterMethodRateVersionDetail>

)

data class EncounterMethodRateVersionDetail(

  // The chance of an encounter to occur.
  val rate: Int,

  // The version of the game in which the encounter can occur with the given chance.
  val version: Version

)

data class PokemonEncounter(

  // The Pokémon being encountered.
  val pokemon: Pokemon,

  // A list of versions and encounters with Pokémon that might happen in the referenced location
  // area.
  val versionDetails: List<VersionEncounterDetail>

)

// Areas used for grouping Pokémon encounters in Pal Park. They're like habitats that are specific
// to [Pal Park](https://bulbapedia.bulbagarden.net/wiki/Pal_Park).
data class PalParkArea(

  // The identifier for this resource.
  val id: Int,

  // The name for this resource.
  val name: String,

  // The name of this resource listed in different languages.
  val names: List<Name>,

  // The name of this resource listed in different languages.
  val pokemonEncounters: List<PalParkEncounterSpecies>

)

data class PalParkEncounterSpecies(

  // The base score given to the player when this Pokémon is caught during a pal park run.
  val baseScore: Int,

  // The base rate for encountering this Pokémon in this pal park area.
  val rate: Int,

  // The Pokémon species being encountered.
  val pokemonSpecies: PokemonSpecies

)

// A region is an organized area of the Pokémon world. Most often, the main difference between
// regions is the species of Pokémon that can be encountered within them.
data class Region(

  // The identifier for this resource.
  val id: Int,

  // A list of locations that can be found in this region.
  val locations: List<Location>,

  // The name for this resource.
  val name: String,

  // The name of this resource listed in different languages.
  val names: List<Name>,

  // The generation this region was introduced in.
  val mainGeneration: Generation,

  // A list of pokédexes that catalogue Pokémon in this region.
  val pokedexes: List<Pokedex>,

  // A list of version groups where this region can be visited.
  val versionGroups: List<VersionGroup>

)