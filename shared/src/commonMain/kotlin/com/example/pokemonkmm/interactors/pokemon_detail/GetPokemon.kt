package com.example.pokemonkmm.interactors.pokemon_detail

import com.example.pokemonkmm.datasource.network.PokemonService
import com.example.pokemonkmm.domain.model.Pokemon
import com.example.pokemonkmm.domain.util.DataState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow


class GetPokemon(private val pokemonService: PokemonService) {

  fun execute(pokemonId: Int): Flow<DataState<Pokemon>> = flow {
    emit(DataState.loading())

    // emit pokemon
    try {

      val pokemon = pokemonService.get(id = pokemonId)
      emit(DataState.data(data = pokemon))

    } catch (e: Exception) {

      emit(DataState.error(message = e.message ?: "Unknown Error"))

    }
  }

}