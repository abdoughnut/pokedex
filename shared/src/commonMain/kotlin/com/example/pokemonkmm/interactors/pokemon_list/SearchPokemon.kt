package com.example.pokemonkmm.interactors.pokemon_list

import com.example.pokemonkmm.datasource.network.PokemonService
import com.example.pokemonkmm.domain.model.Pokemon
import com.example.pokemonkmm.domain.util.DataState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow


class SearchPokemon(private val pokemonService: PokemonService) {

  fun execute(page: Int): Flow<DataState<List<Pokemon>>> = flow {
    emit(DataState.loading())

    // emit pokemon
    try {
      val pokemon = pokemonService.search(page = page)
      emit(DataState.data(data = pokemon))
    } catch (e: Exception) {
      emit(DataState.error(message = e.message ?: "Unknown Error"))
    }
  }

}