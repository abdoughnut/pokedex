object Application {
  const val appId = "com.example.pokemonkmm.android"
  const val versionCode = 1
  const val versionName = "1.0"
  const val minSdk = 24
  const val compileSdk = 31
  const val targetSdk = 31
}