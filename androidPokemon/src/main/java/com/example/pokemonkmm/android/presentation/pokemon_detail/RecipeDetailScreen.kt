package com.example.pokemonkmm.android.presentation.pokemon_detail

import androidx.compose.foundation.layout.Column
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import com.example.pokemonkmm.domain.model.Pokemon


@Composable fun PokemonDetailScreen(pokemon: Pokemon?) {
  if (pokemon == null) {
    Text(text = "Unable to get the details of this pokemon...")
  } else {
    Column {
      Text(text = "PokemonDetailScreen ${pokemon.name}")
    }
  }
}