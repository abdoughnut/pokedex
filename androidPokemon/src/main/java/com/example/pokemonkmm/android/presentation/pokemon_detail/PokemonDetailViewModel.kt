package com.example.pokemonkmm.android.presentation.pokemon_detail

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pokemonkmm.domain.model.Pokemon
import com.example.pokemonkmm.interactors.pokemon_detail.GetPokemon
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class PokemonDetailViewModel @Inject constructor(
  private val savedStateHandle: SavedStateHandle,
  private val getPokemon: GetPokemon,
) : ViewModel() {

  val pokemon: MutableState<Pokemon?> = mutableStateOf(value = null)

  init {
    savedStateHandle.get<Int>("pokemonId")?.let { pokemonId ->
      viewModelScope.launch {
        getPokemon(pokemonId = pokemonId)
      }
    }
  }

  private fun getPokemon(pokemonId: Int) {
    getPokemon.execute(pokemonId = pokemonId).onEach { dataState ->

      println("PokemonDetailVM: ${dataState.isLoading}")

      dataState.data?.let { pokemon ->
        println("PokemonDetailVM: $pokemon")
        this.pokemon.value = pokemon
      }

      dataState.message?.let { message ->
        println("PokemonDetailVM: $message")
      }

    }.launchIn(viewModelScope)
  }

}