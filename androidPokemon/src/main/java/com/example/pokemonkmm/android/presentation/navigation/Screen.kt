package com.example.pokemonkmm.android.presentation.navigation


sealed class Screen(val route: String) {

  object PokemonList : Screen(route = "pokemonList")

  object PokemonDetail : Screen(route = "pokemonDetail")

}