package com.example.pokemonkmm.android.di

import com.example.pokemonkmm.datasource.network.PokemonService
import com.example.pokemonkmm.interactors.pokemon_detail.GetPokemon
import com.example.pokemonkmm.interactors.pokemon_list.SearchPokemon
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module @InstallIn(SingletonComponent::class) object InteractorsModule {

  @Singleton @Provides fun provideSearchPokemon(pokemonService: PokemonService): SearchPokemon {
    return SearchPokemon(pokemonService = pokemonService)
  }

  @Singleton @Provides fun provideGetPokemon(pokemonService: PokemonService): GetPokemon {
    return GetPokemon(pokemonService = pokemonService)
  }

}