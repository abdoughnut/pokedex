package com.example.pokemonkmm.android.presentation.navigation

import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import androidx.hilt.navigation.HiltViewModelFactory
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.pokemonkmm.android.presentation.pokemon_detail.PokemonDetailScreen
import com.example.pokemonkmm.android.presentation.pokemon_detail.PokemonDetailViewModel
import com.example.pokemonkmm.android.presentation.pokemon_list.PokemonListScreen


@Composable fun Navigation() {
  val navController = rememberNavController()
  NavHost(navController = navController, startDestination = Screen.PokemonList.route) {
    composable(route = Screen.PokemonList.route) {
      PokemonListScreen(onSelectedPokemon = { pokemonId ->
        navController.navigate(route = Screen.PokemonDetail.route + "/$pokemonId")
      })
    }
    composable(
      route = Screen.PokemonDetail.route + "/{pokemonId}",
      arguments = listOf(navArgument(name = "pokemonId") {
        type = NavType.IntType
      })
    ) { navBackStackEntry ->
      val factory = HiltViewModelFactory(LocalContext.current, navBackStackEntry)
      val viewModel: PokemonDetailViewModel =
        viewModel(key = "PokemonDetailViewModel", factory = factory)
      PokemonDetailScreen(pokemon = viewModel.pokemon.value)
    }
  }
}