package com.example.pokemonkmm.android.presentation.pokemon_list

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pokemonkmm.interactors.pokemon_list.SearchPokemon
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject


@HiltViewModel
class PokemonListViewModel @Inject constructor(
  private val savedStateHandle: SavedStateHandle,
  private val searchPokemon: SearchPokemon
) : ViewModel() {

  init {
    loadPokemon()
  }

  private fun loadPokemon() {
    searchPokemon.execute(page = 1).onEach { dataState ->

      println("PokemonListVM: ${dataState.isLoading}")

      dataState.data?.let { pokemon ->
        println("PokemonListVM: $pokemon")
      }

      dataState.message?.let { message ->
        println("PokemonListVM: $message")
      }

    }.launchIn(viewModelScope)
  }

}