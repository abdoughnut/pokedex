package com.example.pokemonkmm.android.di

import com.example.pokemonkmm.datasource.network.KtorClientFactory
import com.example.pokemonkmm.datasource.network.PokemonService
import com.example.pokemonkmm.datasource.network.PokemonServiceImpl
import com.example.pokemonkmm.datasource.network.PokemonServiceImpl.Companion.BASE_URL
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import io.ktor.client.HttpClient
import javax.inject.Singleton

@Module @InstallIn(SingletonComponent::class) object NetworkModule {

  @Singleton @Provides fun provideHttpClient(): HttpClient {
    return KtorClientFactory().build()
  }

  @Singleton @Provides fun providePokemonService(httpClient: HttpClient): PokemonService {
    return PokemonServiceImpl(httpClient = httpClient, baseUrl = BASE_URL)
  }
}