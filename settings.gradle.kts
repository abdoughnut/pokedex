pluginManagement {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
    }
}

rootProject.name = "PokemonKmm"
include(":androidPokemon")
include(":shared")